﻿// -----------------------------------------------------------------------
// <copyright file="WordNameAttribute.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;

    /// <summary>
    /// Attribute to specify the Name of a Word.
    /// 
    /// This is nececsary because the naming rules for Forth Words and 
    /// C# methods differ a lot and because we need a link between 
    /// the Lazy class and the inner Forth Word defined.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class WordNameAttribute : Attribute
    {
        /// <summary>
        /// The constructor is called when the attribute is set. 
        /// </summary>
        /// <param name="Word"></param> 
        public WordNameAttribute(String Word)
        {
            this.word = Word;
        }

        /// <summary>
        /// Keep a variable internally.
        /// </summary>
        protected String word;

        /// <summary>
        /// .. and show a copy to the outside world. 
        /// </summary>
        public String Word
        {
            get
            {
                return this.word;
            }
            set
            {
                this.word = value;
            }
        }
    }

}
