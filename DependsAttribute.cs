﻿// -----------------------------------------------------------------------
// <copyright file="DependsAttribute.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;

    /// <summary>
    /// Attribute to specify Dependency between words.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class DependsAttribute : Attribute
    {
        /// <summary>
        /// The constructor is called when the attribute is set. 
        /// </summary>
        /// <param name="Word"></param> 
        public DependsAttribute(String Word)
        {
            this.word = Word;
        }

        /// <summary>
        /// Keep a variable internally.
        /// </summary>
        protected String word;

        /// <summary>
        /// .. and show a copy to the outside world. 
        /// </summary>
        public String Word
        {
            get
            {
                return this.word;
            }
            set
            {
                this.word = value;
            }
        }
    }

}
