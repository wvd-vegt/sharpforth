﻿// -----------------------------------------------------------------------
// <copyright file="Library_Tools.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------
namespace SharpForth
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This class contains the definitions of some additional pure Forth Words.
    /// </summary>
    public static partial class Library
    {
        #region Fields

        /// <summary>
        /// 3560 -
        /// </summary>
        [WordName("?")]
        [WordSet(WordSets.TOOLS)]
        [Depends("(")]
        [Depends("@")]
        [Depends(".")]
        [Syntax("( -- )")]
        internal static String QUESTION = 
        "  ( ? fetches the integer at an address and prints it. )\n" +
        ": ? ( addr -- ) @ . ;\n";

        #endregion Fields
    }
}