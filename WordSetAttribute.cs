﻿// -----------------------------------------------------------------------
// <copyright file="WordNameAttribute.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// Forth Word Sets according to Ansi Forth '94.
    /// </summary>
    public enum WordSets
    {
        /// <summary>
        /// Word set undefined.
        /// </summary>
        [Description("Word set undefined")]
        NONE,

        /// <summary>
        /// Core word set.
        /// Contains 133 words.
        /// </summary>
        [Description("Core word set")]
        CORE,

        /// <summary>
        /// Block word set.
        /// </summary>
        [Description("Block word set")]
        BLOCK,

        /// <summary>
        /// Double word set.
        /// </summary>
        [Description("Double word set")]
        DOUBLE,

        /// <summary>
        /// Exception word set.
        /// </summary>
        [Description("Exception word set")]
        EXCEPTION,

        /// <summary>
        /// Facility word set.
        /// </summary>
        [Description("Facility word set")]
        FACILITY,

        /// <summary>
        /// File-Access word set.
        /// </summary>
        [Description("File-Access word set")]
        FILE,

        /// <summary>
        /// Floating-Point word set.
        /// </summary>
        [Description("Floating-Point word set")]
        FLOATING,

        /// <summary>
        /// Locals word set.
        /// </summary>
        [Description("Locals word set")]
        LOCALS,

        /// <summary>
        /// Memory word set.
        /// </summary>
        [Description("Memory word set")]
        MEMORY,

        /// <summary>
        /// Tools word set.
        /// </summary>
        [Description("Tools word set")]
        TOOLS,

        /// <summary>
        /// Search-Order word set.
        /// </summary>
        [Description("Search-Order word set")]
        SEARCH,

        /// <summary>
        /// String word set.
        /// </summary>
        [Description("String word set")]
        STRING,

        /// <summary>
        /// Core Extension word set.
        /// Contains 46 words.
        /// </summary>
        [Description("Core Extension word set")]
        CORE_EXT,

        /// <summary>
        /// Block Extension word set.
        /// </summary>
        [Description("Block Extension word set")]
        BLOCK_EXT,

        /// <summary>
        /// Double Extension word set.
        /// </summary>
        [Description("Double Extension word set")]
        DOUBLE_EXT,

        /// <summary>
        /// Exception Extension word set.
        /// </summary>
        [Description("Exception Extension word set")]
        EXCEPTION_EXT,

        /// <summary>
        /// Facility Extension word set.
        /// </summary>
        [Description("Facility Extension word set")]
        FACILITY_EXT,

        /// <summary>
        /// File-Access Extension word set.
        /// </summary>
        [Description("File-Access Extension word set")]
        FILE_EXT,

        /// <summary>
        /// Floating-Point Extension word set.
        /// </summary>
        [Description("Floating-Point Extension word set")]
        FLOATING_EXT,

        /// <summary>
        /// Locals Extension word set.
        /// </summary>
        [Description("Locals Extension word set")]
        LOCALS_EXT,

        /// <summary>
        /// Memory Extension word set.
        /// </summary>
        [Description("Memory Extension word set")]
        MEMORY_EXT,

        /// <summary>
        /// Tools Extension word set.
        /// </summary>
        [Description("Tools Extension word set")]
        TOOLS_EXT,

        /// <summary>
        /// Search-Order Extension word set.
        /// </summary>
        [Description("Search-Order Extension word set")]
        SEARCH_EXT,

        /// <summary>
        /// String Extension word set.
        /// </summary>
        [Description("String Extension word set")]
        STRING_EXT,

        /// <summary>
        /// SharpForth Word set.
        /// </summary>
        [Description("SharpForth Word set")]
        SHARPFORTH,

        /// <summary>
        /// User-defined word set.
        /// </summary>
        [Description("User-defined word set")]
        USER,

        /// <summary>
        /// Legacy-defined word set.
        /// </summary>
        [Description("Legacy-defined word set")]
        LEGACY,
    };

    /// <summary>
    /// Attribute to specify the Word Set of a Forth Word.
    /// 
    /// In the Forth 94 Standard, Words are grouped in Word Sets.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class WordSetAttribute : Attribute
    {
        /// <summary>
        /// The constructor is called when the attribute is set. 
        /// </summary>
        /// <param name="wordset">The WordSet</param> 
        public WordSetAttribute(WordSets wordset)
        {
            this.wordset = wordset;
        }

        /// <summary>
        /// Keep a variable internally.
        /// </summary>
        protected WordSets wordset;

        /// <summary>
        /// .. and show a copy to the outside world. 
        /// </summary>
        public WordSets WordSet
        {
            get
            {
                return this.wordset;
            }
            set
            {
                this.wordset = value;
            }
        }
    }

}
