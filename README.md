# README #

SharpForth is a Forth interpreter that is written in 100% C# and is compatible with both Microsoft .Net 4 and Mono.

# Downloads #

Download [location](https://bitbucket.org/wvd-vegt/sharpforth/downloads/SharpForth.application) (Click-Once).

Other downloads [installers](https://wvd-vegt.bitbucket.io/).

### What is SharpForth? ###

SharpForth is a Forth Interpreter that is written in C# without the usual assembly language and low level memory access. It's modelled after JonesForth and some of its derivates like JonesForthInC and bb4wForth.

It was not the purpose to create a high performance Forth implementation or a 100% Forth79/83 compliant system, but a robust environment for experimenting with Forth as a language. 

It's more a proof of concept that Forth can be implemented in pure managed C# code without the usual low level memory access and extremely optimized data structures.

Due to the totally different underlying implementation of core words, the absence of the usual Forth flat memory, the different layout of Forth Words and linked list dictionary some of the Forth features are not implemented or cannot be implemented.

Please note that SharpForth is still being developed and not all features may work according to Forth specs. 

I've been working hard to make it as close to the **1994 ANSI Forth** standard (**ANSI X3.215-1994 / ISO**) as possible by implementing as many as possible of it's word sets. So far **131** of the **133** **CORE** words have been implemented (besides numerous from other sets and legacy ones). **FACILITY** and **DOUBLE** are completely Implemented. Others are almost fully implemented (like **FLOATING**), but some are  partially or not implemented (like **BLOCK**, **EXCEPTION**, **FILE**, **LOCAL** and **SEARCH**).

The implementation of the **FLOATING** word-set is almost fully implemented (exceptions are pointer operations and single precision).

SharpForth contains some specific words like **PI** and **E**, offers debugging facilities with **TRACING**, .**STACK**, .**FSTACK** and **SEE** and has simple functions to control the cursor location in the Console. It also offers built-in documentation of the syntax of words using Forth Stack Notation (**SYNTAX** word). SharpForth can also load Forth files directly from a URL.

### Contribution guidelines ###

* Just create issues and include fixes. 
* Please do **NOT** fork this all over the place !
* 