﻿// -----------------------------------------------------------------------
// <copyright file="Library_Exception.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------
namespace SharpForth
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    /// <summary>
    /// This class contains the definitions of some additional pure Forth Words.
    /// </summary>
    public static partial class Library
    {
        #region Forth Exception Words.

        //
        // Tricky to implement as THROW depends on ABORT and ABORT depends on THROW.
        //

        ///// <summary>
        ///// Exception Handling.
        /////  : FOO ( n -- ) THROW ;",
        ///// 
        /////    : TEST-EXCEPTIONS",
        /////        25 ['] FOO CATCH        \\ execute 25 FOO, catching any exception",
        /////        ?DUP IF",
        /////              .\" called FOO and it threw exception number: \"",
        /////             . CR",
        /////                DROP            \\ we have to drop the argument of FOO (25)",
        /////            THEN",
        /////     ;",
        /////     \\ prints: called FOO and it threw exception number: 25",
        ///// </summary>
        //[WordName("EXCEPTION")]
        //[WordSet(WordSets.EXCEPTION)]
        //[Syntax("EXCEPTION ( -- )")]
        //internal static String _00650_ =
        //    ": EXCEPTION-MARKER\n" +
        //    "    RDROP                   ( drop the original parameter stack pointer )\n" +
        //    "    0                       ( there was no exception, this is the normal return path )\n" +
        //    ";\n";


        //defcode(EXECUTE,0,"EXECUTE")
        //    pop eax             // Get xt into %eax
        //    jmp D [eax]         // and jump to it.
        //    ENDS

        //[WordName("CATCH")]
        //[WordSet(WordSets.EXCEPTION)]
        //[Depends("EXCEPTION-MARKER")]
        //[Depends("EXECUTE")]
        //[Syntax("CATCH ( xt -- exn? )")]
        //internal static String _00660_ =
        //    ": CATCH         ( xt -- exn? )\n" +
        //    "    DSP@ 4+ >R              ( save parameter stack pointer (+4 because of xt) on the return stack )\n" +
        //    "    ['] EXCEPTION-MARKER 4+ ( push the address of the RDROP inside EXCEPTION-MARKER ... )\n" +
        //    "    >R                      ( ... on to the return stack so it acts like a return address )\n" +
        //    "    EXECUTE                 ( execute the nested function )\n" +
        //    ";\n";

        //[WordName("THROW")]
        //[WordSet(WordSets.EXCEPTION)]
        //[Depends("BEGIN")]
        //[Depends("WHILE")]
        //[Depends("CASE")]
        //[Depends("REPEAT")]
        //[Depends("OF")]
        //[Depends("ENDCASE")]
        //[Depends("EXCEPTION-MARKER")]
        //[Syntax("THROW ( n -- )")]
        //internal static String _00670_ =
        //": THROW         ( n -- )\n" +
        //"    ?DUP IF                 ( only act if the exception code <> 0 )\n" +
        //"        RSP@                    ( get return stack pointer )\n" +
        //"        BEGIN\n" +
        //"             DUP R0 @ 4- <           ( RSP < R0 )\n" +
        //"        WHILE\n" +
        //"             DUP @                   ( get the return stack entry )\n" +
        //"             ['] EXCEPTION-MARKER 4+ = IF      ( found the EXCEPTION-MARKER on the return stack )\n" +
        //"                 4+                  ( skip the EXCEPTION-MARKER on the return stack )\n" +
        //"                 RSP!                ( restore the return stack pointer )\n" +
        //"                 \n" +
        //"                 ( Restore the parameter stack. )\n" +
        //"                 DUP DUP DUP         ( reserve some working space so the stack for this word\n" +
        //"                                       doesn't coincide with the part of the stack being restored )\n" +
        //"                 R>                  ( get the saved parameter stack pointer | n dsp )\n" +
        //"                 4-                  ( reserve space on the stack to store n )\n" +
        //"                 SWAP OVER           ( dsp n dsp )\n" +
        //"                 !                   ( write n on the stack )\n" +
        //"                 DSP! EXIT           ( restore the parameter stack pointer, immediately exit )\n" +
        //"             THEN\n" +
        //"             4+\n" +
        //"        REPEAT\n" +
        //"        \n" +
        //"        ( No matching catch - print a message and restart the INTERPRETer. )\n" +
        //"        DROP\n" +
        //"        \n" +
        //"        CASE\n" +
        //"        0 1- OF ( ABORT )\n" +
        //"           .\" ABORTED\" CR\n" +
        //"        ENDOF\n" +
        //"             ( default case )\n" +
        //"             .\" UNCAUGHT THROW \"\n" +
        //"             DUP . CR\n" +
        //"        ENDCASE\n" +
        //"        QUIT\n" +
        //"    THEN\n" +
        //";\n";

        #endregion Forth Exception Words.
    }
}