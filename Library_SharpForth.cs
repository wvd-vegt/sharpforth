﻿// -----------------------------------------------------------------------
// <copyright file="Library_Exception.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------
namespace SharpForth
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This class contains the definitions of some additional pure Forth Words.
    /// </summary>
    public static partial class Library
    {
        #region Fields

        /// <summary>
        /// 3423 - Number Base.
        /// </summary>
        [WordName("BINARY")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("(")]
        [Depends("BASE")]
        [Depends("!")]
        [Syntax("( -- )")]
        internal static String BINARY = ": BINARY 2 BASE ! ;\n";

        /// <summary>
        /// Help.
        /// </summary>
        [WordName("HELP")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("CR")]
        [Depends(".\"")]
        [Syntax("( -- )")]
        internal static String HELP = 
        ": HELP CR\n" +
        " .\" HELP word \" CR\n" +
        " CR\n" +
        " .\" This forth supports a single line editor for your commands and \" CR\n" +
        " .\" supports the following editing keys: \" CR\n" +
        " CR\n" +
        " .\"   · The Ins key toggles character insert/overwrite mode of the editor. \" CR\n" +
        " .\"   · The Backspace key deletes the key to the left of the keyboard cursor, \" CR\n" +
        " .\"     moving the cursor and all chars to the right of the cursor left one \" CR\n" +
        " .\"     position. \" CR\n" +
        " .\"   · The Backspace key deletes the char under the cursor\" CR\n" +
        " .\"   · Left and right arrow keys allow you to move the keyboard cursor in your \" CR\n" +
        " .\"     command. \" CR\n" +
        " .\"   · Up and down arrow keys allow you to recall commands already entered \" CR\n" +
        " .\"     for further editing and execution. \" CR\n" +
        " CR\n" +
        " .\" NOTES: \" CR\n" +
        " .\"   · Command line history is lost upon Quitting the system. \" CR\n" +
        " .\"   · You can quit this Forth with the BYE word. \" CR\n" +
        " .\"   · This forth is case sensitive. \" CR CR\n" +
        ";\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("OCTAL")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("(")]
        [Depends("BASE")]
        [Depends("!")]
        [Syntax("( -- )")]
        internal static String OCTAL = ": OCTAL 8 BASE ! ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'A'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- 65 )")]
        internal static String TICK_A_TICK = ": 'A' [CHAR] A ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("':'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[")]
        [Depends("CHAR")]
        [Depends("[")]
        [Depends("LITERAL")]
        [Syntax("( -- )")]
        internal static String TICK_COLON_TICK = ": ':' [ CHAR : ] LITERAL ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'\\r'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( -- 13 )")]
        internal static String TICK_CR_TICK = ": '\\r' 13 ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'.'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- )")]
        internal static String TICK_DOT_TICK = ": '.' [CHAR] . ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'{'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- 123 )")]
        internal static String TICK_LACOLADE_TICK = ": '{' [CHAR] { ;\n";

        /// <summary>
        /// 3186 - Define some character constants.
        /// </summary>
        [WordName("'\\n'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( -- 10 )")]
        internal static String TICK_LF_TICK = ": '\\n' 10 ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'('")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[")]
        [Depends("CHAR")]
        [Depends("[")]
        [Depends("LITERAL")]
        [Syntax("( -- )")]
        internal static String TICK_LPAREN_TICK = ": '(' [ CHAR ( ] LITERAL ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'-'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Depends(".")]
        [Syntax("( -- )")]
        internal static String TICK_MINUS_TICK = ": '-' [CHAR] - ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'\"'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- )")]
        internal static String TICK_QUOTE_TICK = ": '\"' [CHAR] \" ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'}'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- 125 )")]
        internal static String TICK_RACOLADE_TICK = ": '}' [CHAR] } ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("')'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- )")]
        internal static String TICK_RPAREN_TICK = ": ')' [CHAR] ) ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("';'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[")]
        [Depends("CHAR")]
        [Depends("[")]
        [Depends("LITERAL")]
        [Syntax("( -- )")]
        internal static String TICK_SEMICOLON_TICK = ": ';' [ CHAR ; ] LITERAL ;\n";

        /// <summary>
        /// 
        /// </summary>
        [WordName("'0'")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("[CHAR]")]
        [Syntax("( -- 48 )")]
        internal static String TICK_ZERO_TICK = ": '0' [CHAR] 0 ;\n";

        #endregion Fields
    }
}