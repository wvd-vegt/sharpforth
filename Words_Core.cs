﻿// -----------------------------------------------------------------------
// <copyright file="Words_Core.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Threading;
    using System.Text;

    /// <summary>
    /// This static class contains all built-in Forth Words.
    /// 
    /// When starting the interpreter, 
    /// a piece of Forth Code will add some more Forth Words.
    /// 
    /// Words can be dependend on each other (so load in the correct order) and
    /// are using Lazy&lt;T&gt; pattern so they initialize only when added to the 
    /// Dictionary (and thus in the correct order). 
    /// 
    /// This Lazy pattern is needed as some of the Forth Words in this class are 
    /// defined in Forth, where others are written in pure C#.
    /// 
    /// For some variables (DP, BASE, HERE, STATE and LATEST) some additional mopping up 
    /// is needed (set the address correctly), just after loading the Forth Words.
    /// 
    /// This way these variables can be used in the next Forth Word added during 
    /// initializing the Interpreter.
    /// 
    /// //! NOTE: In order to match Lazy&lt;Dictionary&gt; and the actual Forth Word defined in the 
    /// //!       DictionaryEntry, the WordName Attribute must match the name of the Forth Word defined.
    /// //!       This because Forth Words have a different allowed syntax than C# Identifiers used for the 
    /// //!       Lazy&lt;Dictionary&gt;. It's used for both resolving Depends Attributes and the Forth SYNTAX Word.
    /// </summary>
    public static partial class Words
    {
        #region Forth Core Words

        /// <summary>
        /// 998
        /// </summary>
        [WordName("DROP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- )")]
        internal static Lazy<DictionaryEntry> DROP = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("DROP", 0, delegate
            {
                Int32 n = Forth.POP_P_STACK();

                Forth.NEXT();
            }), true
            );

        /// <summary>
        /// 1002
        /// </summary>
        [WordName("SWAP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- x2 x1 )")]
        internal static Lazy<DictionaryEntry> SWAP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("SWAP", 0, delegate
             {
                 Int32 n2 = Forth.POP_P_STACK();
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push(n2);
                 Forth.PARAM_STACK.Push(n1);

                 Forth.NEXT();
             }), true
             );

        /// <summary>
        /// 1010
        /// </summary>
        [WordName("DUP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x -- x x )")]
        internal static Lazy<DictionaryEntry> DUP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("DUP", 0, delegate
             {
                 Int32 n1 = Forth.PARAM_STACK.Peek();

                 Forth.PARAM_STACK.Push(n1);

                 Forth.NEXT();
             }), true
            );

        /// <summary>
        /// 1014
        /// </summary>
        [WordName("OVER")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- x1 x2 x1 )")]
        internal static Lazy<DictionaryEntry> OVER = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("OVER", 0, delegate
             {
                 Int32 n2 = Forth.POP_P_STACK();
                 Int32 n1 = Forth.PARAM_STACK.Peek();

                 Forth.PARAM_STACK.Push(n2);
                 Forth.PARAM_STACK.Push(n1);

                 Forth.NEXT();
             }), true
            );

        /// <summary>
        /// 1018 (n1 n2 n3 -- n2 n1 n2)
        /// </summary>
        [WordName("ROT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 x3 -- x2 x3 x1 )")]
        internal static Lazy<DictionaryEntry> ROT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("ROT", 0, delegate
             {
                 Int32 n3 = Forth.POP_P_STACK();
                 Int32 n2 = Forth.POP_P_STACK();
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push(n2);
                 Forth.PARAM_STACK.Push(n3);
                 Forth.PARAM_STACK.Push(n1);

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1036 (n1 n2 -- )
        /// </summary>
        [WordName("2DROP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- )")]
        internal static Lazy<DictionaryEntry> TWODROP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("2DROP", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1041 (n1 n2 -- n1 n2 n1 n2)
        /// </summary>
        [WordName("2DUP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- x1 x2 x1 x2 )")]
        internal static Lazy<DictionaryEntry> TWODUP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("2DUP", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1);
                Forth.PARAM_STACK.Push(n2);

                Forth.PARAM_STACK.Push(n1);
                Forth.PARAM_STACK.Push(n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1046 (n1 n2 n3 n4 -- n3 n4 n1 n2)
        /// </summary>
        [WordName("2SWAP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 x3 x4 -- x3 x4 x1 x2 )")]
        internal static Lazy<DictionaryEntry> TWOSWAP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("2SWAP", 0, delegate
            {
                Int32 n4 = Forth.POP_P_STACK();
                Int32 n3 = Forth.POP_P_STACK();
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n3);
                Forth.PARAM_STACK.Push(n4);

                Forth.PARAM_STACK.Push(n1);
                Forth.PARAM_STACK.Push(n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1057 (n1 -- n1 n1 if n1 != 0 | n1 if n1 == 0)
        /// </summary>
        [WordName("?DUP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x -- 0 | x x )")]
        internal static Lazy<DictionaryEntry> QDUP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("?DUP", 0, delegate
            {
                Int32 n1 = Forth.PARAM_STACK.Peek();
                if (n1 != 0)
                {
                    Forth.PARAM_STACK.Push(n1);
                }

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1065 (n1 -- n1+1)
        /// </summary>
        [WordName("1+")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1|u1 -- n2|u2 )")]
        internal static Lazy<DictionaryEntry> INCR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("1+", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 + 1);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1069 (n1 -- n1-1)
        /// </summary>
        [WordName("1-")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1|u1 -- n2|u2 )")]
        internal static Lazy<DictionaryEntry> DECR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("1-", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 - 1);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1081 (n1 n2 -- n1+n2)
        /// </summary>
        [WordName("+")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 -- n3 )")]
        internal static Lazy<DictionaryEntry> ADD = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("+", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 + n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        ///  1086 (n1 n2 -- n1-n2)
        /// </summary>
        [WordName("-")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1|u1 n2|u2 -- n3|u3 )")]
        internal static Lazy<DictionaryEntry> SUB = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("-", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 - n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1086 (n1 n2 -- n1*n2)
        /// </summary>
        [WordName("*")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1|u1 n2|u2 -- n3|u3 )")]
        internal static Lazy<DictionaryEntry> MUL = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("*", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                //! TODO Ignore overflow!

                Forth.PARAM_STACK.Push(n1 * n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1103 (n1 n2 -- n1%n2 n1/n2)
        /// </summary>
        [WordName("/MOD")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 -- n3 n4 )")]
        internal static Lazy<DictionaryEntry> DIVMOD = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("/MOD", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();
                Int32 n3 = 0;

                Int32 n4 = Math.DivRem(n1, n2, out n3);

                Forth.PARAM_STACK.Push(n3);
                Forth.PARAM_STACK.Push(n4);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1143 (n1 n2 -- 1 if n1 == n2 | 0 if n1 != n2)
        /// </summary>
        [WordName("=")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- flag )")]
        internal static Lazy<DictionaryEntry> EQU = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("=", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 == n2 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1151 (n1 n2 -- 1 if n1 &lt; n2 | 0 if n1 &gt;= n2)
        /// </summary>
        [WordName("<")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 -- flag )")]
        internal static Lazy<DictionaryEntry> LT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("<", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 < n2 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1154 (n1 n2 -- 1 if n1 &gt; n2 | 0 if n1 &lt;= n2)
        /// </summary>
        [WordName(">")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 -- flag )")]
        internal static Lazy<DictionaryEntry> GT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(">", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 > n2 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1177 (n1 -- 1 if n1 == 0 | 0 if n1 != 0)
        /// </summary>
        [WordName("0=")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- flag )")]
        internal static Lazy<DictionaryEntry> ZEQU = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("0=", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 == 0 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1186 (n1 -- 1 if n1 &lt; 0 | 0 if n1 &gt;= 0)
        /// </summary>
        [WordName("0<")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- flag )")]
        internal static Lazy<DictionaryEntry> ZLT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("0<", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 < 0 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1204 (n1 n2 -- n1&amp;n2)
        /// </summary>
        [WordName("AND")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- x3 )")]
        internal static Lazy<DictionaryEntry> AND = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("AND", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 & n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1207 (n1 n2 -- n1|n2)
        /// </summary>
        [WordName("OR")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- x3 )")]
        internal static Lazy<DictionaryEntry> OR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("OR", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 | n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1210 (n1 n2 -- n1^n2)
        /// </summary>
        [WordName("XOR")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 x2 -- x3 )")]
        internal static Lazy<DictionaryEntry> XOR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("XOR", 0, delegate
            {
                Int32 n2 = Forth.POP_P_STACK();
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push(n1 ^ n2);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1210 (n1 -- ~n1)
        /// </summary>
        [WordName("INVERT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 -- x2 )")]
        internal static Lazy<DictionaryEntry> INVERT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("INVERT", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PARAM_STACK.Push((Int32)((UInt32)n1 ^ 0xFFFFFFFF));

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1241 ()
        /// </summary>
        [WordName("EXIT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- ) ( R: nest-sys -- ")]
        internal static Lazy<DictionaryEntry> EXIT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("EXIT", 0, delegate
            {
                //! Implemented inside Invoke()/Execute().
            }), true);

        /// <summary>
        /// 1309 ()
        /// </summary>
        [WordName("!")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x a-addr -- )")]
        internal static Lazy<DictionaryEntry> STORE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("!", 0, delegate
            {
                Int32 adr = Forth.POP_P_STACK();
                Int32 dat = Forth.POP_P_STACK();

                switch (Forth.A2M(adr))
                {
                    case Forth.MEM_DEF:
                        Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)] = dat;
                        break;
                    case Forth.MEM_RSP:
                        Forth.DoOutput("MemoryType 1 Not Implemented");
                        break;
                    case Forth.MEM_PRM:
                        Forth.DoOutput("MemoryType 2 Not Implemented");
                        break;
                    case Forth.MEM_WRD:
                        Forth.STRING_BUFFER[Forth.A2A(adr)] = (Char)dat;
                        break;
                }

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1315 ( )
        /// </summary>
        [WordName("@")]
        [WordSet(WordSets.CORE)]
        [Syntax("( a-addr -- x )")]
        internal static Lazy<DictionaryEntry> FETCH = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("@", 0, delegate
            {
                Int32 adr = Forth.POP_P_STACK(); // address to fetch
                Int32 dat = 0;

                switch (Forth.A2M(adr))
                {
                    case Forth.MEM_DEF:
                        dat = Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)];
                        break;
                    case Forth.MEM_RSP:
                        Forth.DoOutput("MemoryType 1 Not Implemented");
                        break;
                    case Forth.MEM_PRM:
                        Forth.DoOutput("MemoryType 2 Not Implemented");
                        break;
                    case Forth.MEM_WRD:
                        dat = (Int32)Forth.STRING_BUFFER[Forth.A2A(adr)];
                        break;
                }

                Forth.PARAM_STACK.Push(dat);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1320 ( )
        /// </summary>
        [WordName("+!")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n|u a-addr -- )")]
        internal static Lazy<DictionaryEntry> ADDSTORE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("+!", 0, delegate
            {
                Int32 adr = Forth.POP_P_STACK(); // address to fetch
                Int32 dat = Forth.POP_P_STACK();

                switch (Forth.A2M(adr))
                {
                    case Forth.MEM_DEF:
                        Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)] += dat;
                        break;
                    case Forth.MEM_RSP:
                        Forth.DoOutput("MemoryType 1 Not Implemented");
                        break;
                    case Forth.MEM_PRM:
                        Forth.DoOutput("MemoryType 2 Not Implemented");
                        break;
                    case Forth.MEM_WRD:
                        Forth.STRING_BUFFER[Forth.A2A(adr)] += (Char)dat;
                        break;
                }

                Forth.NEXT();
            }), true);


        /// <summary>
        /// 1405 ( --- STATE)
        /// </summary>
        [WordName("STATE")]
        [WordSet(WordSets.CORE)]
        [Depends("DOCOL")]
        [Depends("LIT")]
        [Depends("BRANCH")]
        [Depends("EXIT")]
        [Syntax("( -- a-addr )")]
        internal static Lazy<DictionaryEntry> STATE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("STATE", 0, new Int32[]
                {
                    Forth.Locate("DOCOL"),

                    Forth.Locate("LIT"),
                    ((Forth.Dictionary.IndexOf(Forth.Dictionary.Last()) + 1) << 16) + 24,
                    
                    Forth.Locate("BRANCH"), // Jump over DP
                    12,
                    //
                    Forth.Locate("LIT"),
                    Forth.STATE,
                    //
                    Forth.Locate("EXIT")
                }), true);

        /// <summary>
        /// Words I define not in Forth but in C# for convieneance.
        /// </summary>
        [WordName(".")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- )")]
        internal static Lazy<DictionaryEntry> DOT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(".", 0, delegate
            {
                if (Forth.PARAM_STACK.Count != 0)
                {
                    String numerical;
                    if (Forth.DecimalToBase(Forth.POP_P_STACK(), Forth.BASE, out numerical))
                    {
                        Forth.DoOutput("{0} ", numerical);
                    }
                }
                else
                {
                    Forth.DoOutput(Forth.ErrorMarker + "Cannot Pop from Empty Parameter Stack.", true);
                }
            }), true);

        /// <summary>
        /// 1410 ( --- BASE) Must be located after BRANCH/LIT
        /// </summary>
        [WordName("BASE")]
        [WordSet(WordSets.CORE)]
        [Depends("DOCOL")]
        [Depends("LIT")]
        [Depends("BRANCH")]
        [Depends("EXIT")]
        [Syntax("( -- a-addr )")]
        internal static Lazy<DictionaryEntry> BASE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("BASE", 0, new Int32[]
                {
                    Forth.Locate("DOCOL"),

                    Forth.Locate("LIT"),
    
                    //! 24 is 6 words (4*6 bytes) ahead, points to BASE Below, 
                    //! This address Will pe pushed on the PARAM_STACK.
                    ((Forth.Dictionary.IndexOf(Forth.Dictionary.Last()) + 1) << 16) + 24,

                    //! Jump over the LIT BASE
                    Forth.Locate("BRANCH"),
                    12,

                    //! The actual Storage and Initial Value.
                    Forth.Locate("LIT"),
                    Forth.BASE,
                      
                    Forth.Locate("EXIT")
                }), true);

        /// <summary>
        /// 1488 ( )
        /// </summary>
        [WordName(">R")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x -- ) ( R: -- x )")]
        internal static Lazy<DictionaryEntry> TOR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(">R", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Forth.PUSH_R_STACK(n1);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1493 ( )
        /// </summary>
        [WordName("R>")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- x ) ( R: x -- )")]
        internal static Lazy<DictionaryEntry> FROMR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("R>", 0, delegate
            {
                Int32 n1 = Forth.POP_R_STACK();

                Forth.PARAM_STACK.Push(n1);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// bb4wForth
        /// </summary>
        [WordName("R@")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- x ) ( R: x -- x )")]
        internal static Lazy<DictionaryEntry> RSTORE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("R@", 0, delegate
             {
                 Int32 n1 = Forth.POP_R_STACK();

                 Forth.PUSH_R_STACK(n1);
                 Forth.PUSH_P_STACK(n1);

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1705 ( )
        /// </summary>
        [WordName("KEY")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- char )")]
        internal static Lazy<DictionaryEntry> KEY = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("KEY", 0, delegate
            {

                //! 1) Check if buffer contains character, return it.
                //! 2) Check for File Input, append to buffer
                //! 3) Get Line, append to buffer, goto #1.

                Char ch = '\0';

                do
                {
                    ch = Forth.DoInputChar();
                    if (ch != '\0')
                    {
                        Forth.PARAM_STACK.Push(ch);
                    }
                } while (ch == '\0');

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1736 ( char -- )
        /// </summary>
        [WordName("EMIT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x -- )")]
        internal static Lazy<DictionaryEntry> EMIT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("EMIT", 0, delegate
            {
                Int32 ch = Forth.POP_P_STACK();
                Forth.DoOutput(String.Format("{0}", "" + (Char)ch), false);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1813 ( )
        /// 
        /// WORD scans the buffer for input.
        /// 
        /// <remarks>
        /// WORD does use not return a counted string 'c-addr' but 'a-addr len'.
        /// </remarks>
        /// </summary>
        [WordName("WORD")]
        [WordSet(WordSets.CORE)]
        [Syntax("( char\"<chars>ccc<char>\" -- a-addr u )")]
        internal static Lazy<DictionaryEntry> WORD = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("WORD", 0, delegate
            {
                Int32 WORD_BUFFER_START = Forth.STRING_BUFFER.Count;

                Char key = Forth.DoInputChar();

                // 1) Skip Leading Whitespace.
                while (Char.IsWhiteSpace(key) || key == '\0' || key == '\t' || key == '\r' || key == '\n')
                {
                    key = Forth.DoInputChar();
                }

                do
                {
                    // 2) Skip comment until end of line.
                    if (key == '\\')
                    {
                        while (key != '\n')
                        {
                            key = Forth.DoInputChar();
                        }

                        key = Forth.DoInputChar();

                        // 3) Skip Leading Whitespace.
                        while (Char.IsWhiteSpace(key) || key == '\0' || key == '\t' || key == '\r' || key == '\n')
                        {
                            // 4) Bail-out if no more input.
                            if (!Forth.DoInputAvailable())
                            {
                                key = '\0';
                                break;
                            }
                            else
                            {
                                key = Forth.DoInputChar();
                            }
                        }
                    }
                } while (key == '\\');

                //if (key != '\0')
                //{
                //    Forth.STRING_BUFFER.Add(key);
                //}

                while (Forth.DoInputAvailable()) //Forth.STRING_BUFFER.Count < Forth.WORD_BUFFER_SIZE
                {
                    if (Char.IsWhiteSpace(key) || key == '\0' /*|| key == '\r'*/)
                    {
                        break;
                    }
                    else
                    {
                        Forth.STRING_BUFFER.Add(key);
                    }

                    key = Forth.DoInputChar();
                }

                Forth.PARAM_STACK.Push(WORD_BUFFER_START | Forth.MEM_WRD);
                Forth.PARAM_STACK.Push(Forth.STRING_BUFFER.Count - WORD_BUFFER_START);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 1973
        /// 
        /// FIND returns either the dictionary pointer or 0.
        /// It expects start and length on the stack (WORD output alike).
        /// 
        /// <remarks>
        /// FIND does not use counted strings 'c-addr' but 'a-addr u'.
        /// </remarks>
        /// </summary>
        [WordName("FIND")]
        [WordSet(WordSets.CORE)]
        [Syntax("( c-addr -- c-addr 0 | xt 1 | xt-1)")]
        internal static Lazy<DictionaryEntry> FIND = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("FIND", 0, delegate
            {
                //! Expect a STRING_BUFFER Pointer.
                //
                Int32 length = Forth.POP_P_STACK();
                if (length > 0)
                {
                    Int32 start = Forth.POP_P_STACK() & Forth.MTM;

                    // Look up in the dictionary.
                    Forth.PARAM_STACK.Push(Forth.Locate(Forth.FetchWord(start, length)));
                }
                else
                {
                    Forth.PARAM_STACK.Push(0);
                }
                Forth.NEXT();
            }), true);

        /// <summary>
        /// 2328
        /// </summary>
        [WordName("IMMEDIATE")]
        [WordSet(WordSets.CORE)]
        [Depends("LATEST")]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> IMMEDIATE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("IMMEDIATE", Forth.FORTH_IMMED_MASK, delegate
            {
                Forth.Dictionary[Forth.A2I(Forth.LATEST)].ToggleImmed();

                Forth.NEXT();
            }), true);

        /// <summary>
        /// Used instead of HEADER.
        /// 
        /// Words not starting with DOCOL push their address instead of being executed.
        /// </summary>
        [WordName("CREATE")]
        [WordSet(WordSets.CORE)]
        [Depends("WORD")]
        [Depends("(CREATE)")]
        [Syntax("( \"spaces<name>\" -- )")]
        internal static Lazy<DictionaryEntry> CREATE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("CREATE", 0, delegate
            {
                WORD.Value.Invoke();
                PAREN_CREATE.Value.Invoke();
            }), true);

        /// <summary>
        /// 2265
        /// </summary>
        [WordName(",")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x -- )")]
        internal static Lazy<DictionaryEntry> COMMA = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(",", 0, delegate
            {
                Int32 ndx = Forth.POP_P_STACK();

                Forth.Dictionary.Last().Definition.Add(ndx);

                Forth.DP = Forth.LATEST + (Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Count << 2) | Forth.MEM_DEF;
            }), true);

        /// <summary>
        /// 2290
        /// </summary>
        [WordName("[")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> LBRAC = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("[", Forth.FORTH_IMMED_MASK, delegate
            {
                Forth.STATE = Forth.IMMEDIATE_MODE;

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 2294
        /// </summary>
        [WordName("]")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> RBRAC = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("]", 0, delegate
            {
                Forth.STATE = Forth.COMPILE_MODE;

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 2400
        /// 
        /// <remarks>
        /// Can only be called when compiled into Immediate Forth Words! So 
        /// 
        ///     : T ['] DUP , ; 
        ///     T
        ///     
        /// Fails.
        /// </remarks>
        /// </summary>
        [WordName("[']")]
        [WordSet(WordSets.CORE)]
        [Depends("WORD")]
        [Depends("FIND")]
        [Syntax("( \"<spaces>name\" -- ) ( --xt )")]
        internal static Lazy<DictionaryEntry> BRACKET_TICK = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("[']", Forth.FORTH_IMMED_MASK, delegate
            {
                WORD.Value.Invoke();
                FIND.Value.Invoke();

                switch (Forth.STATE)
                {
                    case Forth.COMPILE_MODE:
                        Forth.Dictionary.Last().Definition.Add(Forth.Locate("LIT"));
                        Forth.Dictionary.Last().Definition.Add(Forth.POP_P_STACK());

                        break;

                    case Forth.IMMEDIATE_MODE:
                        Forth.PARAM_STACK.Push(Forth.PARAM_STACK.Pop());
                        break;
                }

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 2410
        /// </summary>
        [WordName(":")]
        [WordSet(WordSets.CORE)]
        [Depends("DOCOL")]
        [Depends("CREATE")]
        [Depends("LIT")]
        [Depends(",")]
        [Depends("LATEST")]
        [Depends("@")]
        [Depends("HIDDEN")]
        [Depends("]")]
        [Depends("EXIT")]
        [Syntax("( C: \"<spaces>name\" ) ( S: -- xt )")]
        internal static Lazy<DictionaryEntry> COLON = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(":", 0, new Int32[]
            {
                Forth.Locate("DOCOL"),

                //! CREATE the dictionary entry / header
                Forth.Locate("CREATE"),

                //! Append DOCOL (the codeword).
                Forth.Locate("LIT"), Forth.Locate("DOCOL"), Forth.Locate(","),

                //! Make the word hidden
                Forth.Locate("LATEST"), Forth.Locate("@"), Forth.Locate("HIDDEN"),

                //! Go into compile mode.
                Forth.Locate("]"),

                //! Return from the function.
                Forth.Locate("EXIT")
            }), true);

        /// <summary>
        /// 2423
        /// </summary>
        [WordName(";")]
        [WordSet(WordSets.CORE)]
        [Depends("LIT")]
        [Depends("EXIT")]
        [Depends(",")]
        [Depends("LATEST")]
        [Depends("@")]
        [Depends("HIDDEN")]
        [Depends("[")]
        [Syntax("( C:colon-sys -- )")]
        internal static Lazy<DictionaryEntry> SEMICOLON = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(";", Forth.FORTH_IMMED_MASK, new Int32[]
            {
                Forth.Locate("DOCOL"),

                //! Append EXIT (the codeword).
                Forth.Locate("LIT"), Forth.Locate("EXIT"), Forth.Locate(","),

                //! Toggle hidden flag -- unhide the word (see below for definition).
                Forth.Locate("LATEST"), Forth.Locate("@"), Forth.Locate("HIDDEN"),

                //! Go into IMMEDIATE mode.
                Forth.Locate("["),

                //! Return from the function.
                Forth.Locate("EXIT")
            }), true);

        /// <summary>
        /// 2516
        /// </summary>
        [WordName("TYPE")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> TYPE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("TYPE", 0, delegate
            {
                Int32 len = Forth.POP_P_STACK();
                Int32 adr = Forth.POP_P_STACK();

                Int32ByteUnion union = new Int32ByteUnion(0);

                for (Int32 i = 0; i < len; i++)
                {
                    Int32 ndx = adr + i;
                    switch (Forth.A2M(ndx))
                    {
                        case Forth.MEM_DEF:
                            union.iVal = Forth.Dictionary[Forth.A2I(ndx)].Definition[Forth.A2D(ndx)];
                            Forth.DoOutput("{0}", (Char)union[i % 4]);
                            break;
                        case Forth.MEM_PRM:
                            Forth.DoOutput("MemoryType 1 Not Implemented");
                            break;
                        case Forth.MEM_RSP:
                            Forth.DoOutput("MemoryType 2 Not Implemented");
                            break;
                        case Forth.MEM_WRD:
                            Forth.DoOutput("{0}", Forth.STRING_BUFFER[Forth.A2A(ndx)]);
                            break;
                    }
                }

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 2758
        /// </summary>
        [WordName("QUIT")]
        [WordSet(WordSets.CORE)]
        [Depends("WELCOME")]
        [Depends("INTERPRET")]
        [Syntax("( -- ) ( R: i*x -- )")]
        internal static Lazy<DictionaryEntry> QUIT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("QUIT", 0, delegate
            {
                //Locate("R0"), Locate("@"), Locate("RSP!") ,
                Forth.RETURN_STACK.Clear();
                Forth.PARAM_STACK.Clear();
                Forth.STRING_BUFFER.Clear();
                Forth.START_TICKS = DateTime.Now.Ticks;

                WELCOME.Value.Invoke();

                while (!Forth.Shutdown)
                {
                    try
                    {
                        INTERPRET.Value.Invoke();
                    }
                    catch (AbortException ab)
                    {
                        Debug.Print("ABORT: {0}", ab.Message);

                        if (!String.IsNullOrEmpty(ab.Message))
                        {
                            Forth.DoOutput(Forth.ErrorMarker + "[" + ab.Code + "] - '" + ab.Message + "' ");
                        }

                        Forth.InitForth();
                    }
                    catch (Exception e)
                    {
                        Forth.DoOutput(Forth.ErrorMarker + ".NET Exception - " + e.Message, true);
                        Forth.DoOutput(Forth.ErrorMarker + "                 " + e.Source + " " + e.StackTrace[0].ToString(), true);

                        Forth.InitForth();

                        WELCOME.Value.Invoke();
                    }
                }

                //! Not Shown!
                Forth.DoOutput("SharpForth is shutting down.", true);
            }), true);

        /// <summary>
        ///
        /// </summary>
        [WordName("CHAR")]
        [WordSet(WordSets.CORE)]
        [Depends("WORD")]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> CHAR = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("CHAR", 0, delegate
            {
                WORD.Value.Invoke();

                Int32 length = Forth.POP_P_STACK();
                Int32 start = Forth.POP_P_STACK();

                String word = Forth.FetchWord(start, length, true);
                Char first = word.First();
                Forth.PARAM_STACK.Push(first);

                Forth.NEXT();
            }), true);

        /// <summary>
        /// 3626
        /// </summary>
        [WordName("S\"")]
        [WordSet(WordSets.CORE)]
        [Depends("LITSTRING")]
        [Depends("DOCOL")]
        [Depends("EXIT")]
        [Depends("DATA-MARKER")]
        [Syntax("( \"ccc<quote>\" -- )")]
        internal static Lazy<DictionaryEntry> DEFINE_STRING = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("S\"", Forth.FORTH_IMMED_MASK, delegate
            {
                switch (Forth.STATE)
                {
                    case Forth.COMPILE_MODE:
                        {
                            Forth.Dictionary[Forth.Dictionary.Count - 1].Definition.Add(Forth.Locate("LITSTRING"));

                            //! Save Length Location so we can update.
                            Int32 ndx = Forth.Dictionary.Last().Definition.Count;
                            Forth.Dictionary.Last().Definition.Add(0);

                            Char key = (Char)0;

                            Int32ByteUnion val = new Int32ByteUnion(0);

                            Int32 len = 0;
                            do
                            {
                                key = Forth.DoInputChar();

                                if (key != '\"')
                                {
                                    val[len % 4] = (byte)key;

                                    if (len % 4 == 3)
                                    {
                                        Forth.Dictionary.Last().Definition.Add(val.iVal);
                                        val.iVal = 0;
                                    }

                                    len++;
                                }

                            } while (key != '\"');

                            if (len % 4 != 0)
                            {
                                Forth.Dictionary.Last().Definition.Add(val.iVal);
                                val.iVal = 0;
                            }

                            Forth.Dictionary.Last().Definition[ndx] = len;
                        }
                        break;

                    case Forth.IMMEDIATE_MODE:
                        {
                            Forth.Dictionary.Add(new DictionaryEntry(Forth.NONAME, Forth.FORTH_HIDDEN_MASK));

                            Forth.Dictionary.Last().Definition.Add(Forth.Locate("DOCOL"));
                            Forth.Dictionary.Last().Definition.Add(Forth.Locate("EXIT"));
                            Forth.Dictionary.Last().Definition.Add(Forth.Locate("DATA-MARKER"));

                            Int32 ndx = Forth.Dictionary.Count - 1;
                            Int32 adr = Forth.I2A(Forth.Dictionary.Count - 1) + (3 << 2);

                            Int32 start = Forth.STRING_BUFFER.Count;
                            String s = String.Empty;

                            Char key = (Char)0;

                            Int32ByteUnion val = new Int32ByteUnion(0);

                            Int32 len = 0;
                            do
                            {
                                key = Forth.DoInputChar();

                                if (key != '\"')
                                {
                                    val[len % 4] = (byte)key;

                                    if (len % 4 == 3)
                                    {
                                        Forth.Dictionary.Last().Definition.Add(val.iVal);
                                        val.iVal = 0;
                                    }

                                    len++;
                                }

                            } while (key != '\"');

                            if (len % 4 != 0)
                            {
                                Forth.Dictionary.Last().Definition.Add(val.iVal);
                                val.iVal = 0;
                            }

                            Forth.LATEST = ndx;
                            Forth.DP = Forth.LATEST + (Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Count << 2) | Forth.MEM_DEF;

                            Forth.PUSH_P_STACK(adr);
                            Forth.PUSH_P_STACK(len);
                        }
                        break;
                }

                Forth.NEXT();
            }), true);

        /// <summary>
        /// Returns Depth of Parameter Stack.
        /// </summary>
        [WordName("DEPTH")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- +n )")]
        internal static Lazy<DictionaryEntry> DEPTH = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("DEPTH", 0, delegate
            {
                Forth.PARAM_STACK.Push(Forth.PARAM_STACK.Count);

                Forth.NEXT();
            }), true);

        //! EXCEPTIONS:

        // : ' ( "<spaces>name" -- xt ) (WORD) FIND >CFA ; 

        //: EXCEPTION-MARKER
        //    RDROP			( drop the original parameter stack pointer )
        //    0			( there was no exception, this is the normal return path )
        //;

        //: CATCH		( xt -- exn? )
        //    DSP@ 4+ >R		( save parameter stack pointer (+4 because of xt) on the return stack )
        //    ' EXCEPTION-MARKER 4+	( push the address of the RDROP inside EXCEPTION-MARKER ... )
        //    >R			( ... on to the return stack so it acts like a return address )
        //    EXECUTE			( execute the nested function )
        //;

        //: THROW		( n -- )
        //    ?DUP IF			( only act if the exception code <> 0 )
        //        RSP@ 			( get return stack pointer )
        //        BEGIN
        //            DUP R0 4- <		( RSP < R0 )
        //        WHILE
        //            DUP @			( get the return stack entry )
        //            ' EXCEPTION-MARKER 4+ = IF	( found the EXCEPTION-MARKER on the return stack )
        //                4+			( skip the EXCEPTION-MARKER on the return stack )
        //                RSP!			( restore the return stack pointer )

        //                ( Restore the parameter stack. )
        //                DUP DUP DUP		( reserve some working space so the stack for this word
        //                              doesn't coincide with the part of the stack being restored )
        //                R>			( get the saved parameter stack pointer | n dsp )
        //                4-			( reserve space on the stack to store n )
        //                SWAP OVER		( dsp n dsp )
        //                !			( write n on the stack )
        //                DSP! EXIT		( restore the parameter stack pointer, immediately exit )
        //            THEN
        //            4+
        //        REPEAT

        //        ( No matching catch - print a message and restart the INTERPRETer. )
        //        DROP

        //        CASE
        //        0 1- OF	( ABORT )
        //            ." ABORTED" CR
        //        ENDOF
        //            ( default case )
        //            ." UNCAUGHT THROW "
        //            DUP . CR
        //        ENDCASE
        //        QUIT
        //    THEN
        //;

        //: ABORT		( -- )
        //    0 1- THROW
        //;

        [WordName("CONSTANT")]
        [WordSet(WordSets.CORE)]
        [Depends("CREATE")]
        [Depends("DOCOL")]
        [Depends("LIT")]
        [Depends("EXIT")]
        [Syntax("( x\"<spaces>name\" -- )")]
        internal static Lazy<DictionaryEntry> CONSTANT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("CONSTANT", 0, delegate
             {
                 CREATE.Value.Invoke();

                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("DOCOL"));
                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("LIT"));
                 Forth.Dictionary.Last().Definition.Add(Forth.POP_P_STACK());

                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("EXIT"));

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1410
        /// </summary>
        [WordName("VARIABLE")]
        [WordSet(WordSets.CORE)]
        [Depends("CREATE")]
        [Depends("LIT")]
        [Depends("BRANCH")]
        [Depends("EXIT")]
        [Depends("DATA-MARKER")]
        [Depends(",")]
        [Syntax("( -- addr )")]
        internal static Lazy<DictionaryEntry> VARIABLE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("VARIABLE", 0, delegate
                {
                    CREATE.Value.Invoke();

                    Forth.Dictionary.Last().Definition.Add(Forth.Locate("DOCOL"));
                    Forth.Dictionary.Last().Definition.Add(Forth.Locate("LIT"));

                    //! 12 is 3 words (4*3 bytes) ahead, points to Storage Area Below,
                    //! This address Will pe pushed on the PARAM_STACK.
                    Forth.Dictionary.Last().Definition.Add(((Forth.Dictionary.IndexOf(Forth.Dictionary.Last())) << 16) + 20);
                    Forth.Dictionary.Last().Definition.Add(Forth.Locate("EXIT"));
                    Forth.Dictionary.Last().Definition.Add(Forth.Locate("DATA-MARKER"));

                    //! The Actual Storage Area.
                    Forth.Dictionary.Last().Definition.Add(0);

                    Forth.NEXT();
                }), true);

        /// <summary>
        /// Just a dummy marker to instruct SEE to start ouputting the rest of the Forth Word as Mumeric Array.
        /// </summary>
        [WordName("ALLOT")]
        [WordSet(WordSets.CORE)]
        [Depends("DOCOL")]
        [Depends("LIT")]
        [Depends(",")]
        [Depends("EXIT")]
        [Depends("DATA-MARKER")]
        [Syntax("( n -- )")]
        internal static Lazy<DictionaryEntry> ALLOT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("ALLOT", 0, delegate
             {
                 Int32 cnt = Forth.POP_P_STACK();

                 //! If an empty word, turn it into a VARIABLE on-the-fly.
                 if (Forth.Dictionary.Last().Definition.Count == 0)
                 {
                     Forth.Dictionary.Last().Definition.Add(Forth.Locate("DOCOL"));
                     Forth.Dictionary.Last().Definition.Add(Forth.Locate("LIT"));

                     //! 12 is 3 words (4*3 bytes) ahead, points to Storage Area Below,
                     //! This address Will pe pushed on the PARAM_STACK.
                     Forth.Dictionary.Last().Definition.Add(((Forth.Dictionary.IndexOf(Forth.Dictionary.Last())) << 16) + 20);
                     Forth.Dictionary.Last().Definition.Add(Forth.Locate("EXIT"));
                     Forth.Dictionary.Last().Definition.Add(Forth.Locate("DATA-MARKER"));
                 }

                 //! Round number of bytes to a 4 fold and turn it into Integer Count by dividing by 4.
                 //! Thus 3 ALLOT becomes 4 or 1 CELL.
                 cnt = (cnt + 3) & ~3;
                 cnt = cnt / 4;

                 //! If it already contains stuff, just appand storage.
                 for (Int32 i = 0; i < cnt; i++)
                 {
                     Forth.Dictionary.Last().Definition.Add(0);
                 }

             }), true);

        /*
         * 
         * ANS Forth DO, LOOP, +LOOP, UNLOOP, I, and J execution semantics.
         * 
         */


        /// <summary>
        /// 2848
        /// </summary>
        [WordName("(+LOOP)")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- ) ( R: loop-sys1 -- | loop-sys2 )")]
        internal static Lazy<DictionaryEntry> PAREN_PLUS_LOOP = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("(+LOOP)", 0, delegate
              {
                  //! Implemented inside Invoke()/Execute().

              }), true);

        /// <summary>
        /// 2884
        /// 
        /// I -- Push Inner most loop index
        ///
        /// n|u is a copy of the current (innermost) loop index. An ambiguous condition exists
        /// if the loop control parameters are unavailable.
        /// </summary>
        [WordName("I")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- n|u ) ( R:  loop-sys -- loop-sys )")]
        internal static Lazy<DictionaryEntry> I = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("I", 0, delegate
              {
                  Int32 i = Forth.POP_R_STACK();

                  Forth.PUSH_P_STACK(i);

                  Forth.PUSH_R_STACK(i);
              }), true);

        /// <summary>
        /// 2895 
        /// 
        /// J -- Push next-outer loop index
        ///     
        /// n|u is a copy of the next-outer loop index. An ambiguous condition exists if the
        /// loop control parameters of the next-outer loop, loop-sys1, are unavailable.
        /// </summary>
        [WordName("J")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- n|u ) ( R: loop-sys1 loop-sys2 -- loop-sys1 loop-sys2 )")]
        internal static Lazy<DictionaryEntry> J = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("J", 0, delegate
              {
                  Int32 i = Forth.POP_R_STACK();
                  Int32 j = Forth.POP_R_STACK();
                  Int32 k = Forth.POP_R_STACK();

                  Forth.PUSH_P_STACK(k);

                  Forth.PUSH_R_STACK(k);
                  Forth.PUSH_R_STACK(j);
                  Forth.PUSH_R_STACK(i);
              }), true);

        /// <summary>
        /// 2895
        /// 
        /// UNLOOP
        ///
        /// Discard the loop-control parameters for the current nesting level.
        /// An UNLOOP is required for each nesting level before the definition may be EXITed.
        /// An ambiguous condition exists if the loop-control parameters are unavailable.
        /// </summary>
        [WordName("UNLOOP")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- ) ( R: loop-sys -- )")]
        internal static Lazy<DictionaryEntry> UNLOOP = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("UNLOOP", 0, delegate
              {
                  Forth.POP_R_STACK();
                  Forth.POP_R_STACK();
              }), true);

        /// <summary>
        /// 1340 Stores a Byte.
        /// </summary>
        [WordName("C!")]
        [WordSet(WordSets.CORE)]
        [Syntax("( char c-addr -- )")]
        internal static Lazy<DictionaryEntry> STOREBYTE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("C!", 0, delegate
             {
                 Int32 adr = Forth.POP_P_STACK();
                 Int32 dat = Forth.POP_P_STACK();

                 switch (Forth.A2M(adr))
                 {
                     case Forth.MEM_DEF:
                         Int32ByteUnion val = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)]);
                         val[adr % 4] = (byte)dat;
                         Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)] = val.iVal;
                         break;
                     case Forth.MEM_RSP:
                         Forth.DoOutput("MemoryType 1 Not Implemented");
                         break;
                     case Forth.MEM_PRM:
                         Forth.DoOutput("MemoryType 2 Not Implemented");
                         break;
                     case Forth.MEM_WRD:
                         Forth.STRING_BUFFER[Forth.A2A(adr)] = (Char)dat;
                         break;
                 }

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1346 Fetches a Byte/Char.
        /// </summary>
        [WordName("C@")]
        [WordSet(WordSets.CORE)]
        [Syntax("( c-addr -- char )")]
        internal static Lazy<DictionaryEntry> FETCHBYTE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("C@", 0, delegate
             {
                 Int32 adr = Forth.POP_P_STACK();

                 switch (Forth.A2M(adr))
                 {
                     case Forth.MEM_DEF:
                         Forth.PUSH_P_STACK(new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)])[adr % 4]);
                         break;
                     case Forth.MEM_RSP:
                         Forth.DoOutput("MemoryType 1 Not Implemented");
                         break;
                     case Forth.MEM_PRM:
                         Forth.DoOutput("MemoryType 2 Not Implemented");
                         break;
                     case Forth.MEM_WRD:
                         Forth.PUSH_P_STACK(Forth.STRING_BUFFER[Forth.A2A(adr)]);
                         break;
                 }

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// Stores a Character into the Word Definition.
        /// </summary>
        [WordName("C,")]
        [WordSet(WordSets.CORE)]
        [Syntax("( char -- )")]
        internal static Lazy<DictionaryEntry> CSTORE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("C,", 0, delegate
             {
                 if (Forth.Dictionary[Forth.A2I(Forth.DP)].Definition.Count == Forth.A2D(Forth.DP))
                 {
                     Forth.Dictionary[Forth.A2I(Forth.DP)].Definition.Add(0);
                 }

                 Int32ByteUnion dat = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(Forth.DP)].Definition[Forth.A2D(Forth.DP)]);
                 dat[Forth.DP % 4] = (Byte)Forth.POP_P_STACK();
                 Forth.Dictionary[Forth.A2I(Forth.DP)].Definition[Forth.A2D(Forth.DP)] = dat.iVal;

                 Forth.DP++;

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// Fills a region with a character.
        /// </summary>
        [WordName("FILL")]
        [WordSet(WordSets.CORE)]
        [Syntax("( c-addr u char -- )")]
        internal static Lazy<DictionaryEntry> FILL = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("FILL", 0, delegate
             {
                 Int32 chr = Forth.POP_P_STACK();
                 Int32 len = Forth.POP_P_STACK();
                 Int32 adr = Forth.POP_P_STACK();

                 for (Int32 i = 0; i < len; i++)
                 {
                     switch (Forth.A2M(adr))
                     {
                         case Forth.MEM_DEF:
                             Int32ByteUnion val = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)]);
                             val[adr % 4] = (Byte)chr;
                             Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)] = val.iVal;
                             break;
                         case Forth.MEM_RSP:
                             Forth.DoOutput("MemoryType 1 Not Implemented");
                             break;
                         case Forth.MEM_PRM:
                             Forth.DoOutput("MemoryType 2 Not Implemented");
                             break;
                         case Forth.MEM_WRD:
                             Forth.STRING_BUFFER[Forth.A2A(adr)] = (Char)chr;
                             break;
                     }
                     adr++;
                 }

                 Forth.NEXT();
             }), true);


        /// <summary>
        /// Fetches Length of a Counted String.
        /// </summary>
        [WordName("COUNT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( c-addr1 -- c-addr2 u )")]
        internal static Lazy<DictionaryEntry> COUNT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("COUNT", 0, delegate
             {
                 Int32 adr = Forth.POP_P_STACK();
                 Int32 len = 0;

                 switch (Forth.A2M(adr))
                 {
                     case Forth.MEM_DEF:
                         len = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)])[adr % 4];
                         adr++;
                         break;
                     case Forth.MEM_RSP:
                         Forth.DoOutput("MemoryType 1 Not Implemented");
                         break;
                     case Forth.MEM_PRM:
                         Forth.DoOutput("MemoryType 2 Not Implemented");
                         break;
                     case Forth.MEM_WRD:
                         len = (Byte)Forth.STRING_BUFFER[Forth.A2A(adr)];
                         break;
                 }

                 Forth.PUSH_P_STACK(adr);
                 Forth.PUSH_P_STACK(len);

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// Moves a Cell Range. 
        /// </summary>
        [WordName("MOVE")]
        [WordSet(WordSets.CORE)]
        [Syntax("( addr1 addr2 u -- )")]
        internal static Lazy<DictionaryEntry> MOVE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("MOVE", 0, delegate
             {
                 Int32 cnt = Forth.POP_P_STACK();
                 Int32 dst = Forth.POP_P_STACK();
                 Int32 src = Forth.POP_P_STACK();

                 for (Int32 i = 0; i < cnt; i++)
                 {
                     Byte dat = 0;
                     switch (Forth.A2M(src))
                     {
                         case Forth.MEM_DEF:
                             Int32ByteUnion val = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(src)].Definition[Forth.A2D(src)]);
                             dat = val[src % 4];
                             break;
                         case Forth.MEM_RSP:
                             Forth.DoOutput("MemoryType 1 Not Implemented");
                             break;
                         case Forth.MEM_PRM:
                             Forth.DoOutput("MemoryType 2 Not Implemented");
                             break;
                         case Forth.MEM_WRD:
                             dat = (Byte)Forth.STRING_BUFFER[Forth.A2A(src)];
                             break;
                     }

                     switch (Forth.A2M(dst))
                     {
                         case Forth.MEM_DEF:
                             Int32ByteUnion val = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(dst)].Definition[Forth.A2D(dst)]);
                             val[dst % 4] = dat;
                             Forth.Dictionary[Forth.A2I(dst)].Definition[Forth.A2D(dst)] = val.iVal;
                             break;
                         case Forth.MEM_RSP:
                             Forth.DoOutput("MemoryType 1 Not Implemented");
                             break;
                         case Forth.MEM_PRM:
                             Forth.DoOutput("MemoryType 2 Not Implemented");
                             break;
                         case Forth.MEM_WRD:
                             Forth.STRING_BUFFER[Forth.A2A(dst)] = (Char)dat;
                             break;
                     }

                     src++;
                     dst++;
                 }

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// bb4wForth n1 SHL 1
        /// </summary>
        [WordName("2*")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 -- x2 )")]
        internal static Lazy<DictionaryEntry> TWOMUL = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("2*", 0, delegate
             {
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PUSH_P_STACK(n1 << 1);
             }), true);

        /// <summary>
        /// bb4wForth n1 / 2
        /// </summary>
        [WordName("2/")]
        [WordSet(WordSets.CORE)]
        [Syntax("( x1 -- x2 )")]
        internal static Lazy<DictionaryEntry> TWODIV = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("2/", 0, delegate
             {
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PUSH_P_STACK(n1 >> 1);
             }), true);

        /// <summary>
        /// bb4wForth Unsigned GT.
        /// </summary>
        [WordName("U<")]
        [WordSet(WordSets.CORE)]
        [Syntax("(n1 n2 -- flag )")]
        internal static Lazy<DictionaryEntry> ULT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("U<", 0, delegate
             {
                 Int32 n2 = Forth.POP_P_STACK();
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push((UInt32)n1 < (UInt32)n2 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                 Forth.NEXT();
             }), true);

        [WordName("EXECUTE")]
        [WordSet(WordSets.CORE)]
        [Syntax("( i*x xt -- j*x )")]
        internal static Lazy<DictionaryEntry> EXECUTE = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("EXECUTE", 0, delegate
            {
                Int32 adr = Forth.POP_P_STACK();
                if (Forth.A2I(adr) > 0)
                {
                    Forth.Dictionary[Forth.A2I(adr)].Invoke();
                }
            }), true);

        [WordName("*/")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 n3 -- n4 )")]
        internal static Lazy<DictionaryEntry> MULDIV = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("*/", 0, delegate
            {
                Int64 n1 = (Int64)Forth.POP_P_STACK();
                Int64 n2 = (Int64)Forth.POP_P_STACK();
                Int64 n3 = (Int64)Forth.POP_P_STACK();

                Forth.PUSH_P_STACK((Int32)(n3 * n2 / n1));
            }), true);

        [WordName("*/MOD")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 n3 -- n4 n5 )")]
        internal static Lazy<DictionaryEntry> MULDIVMOD = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("*/MOD", 0, delegate
            {
                Int64 n1 = (Int64)Forth.POP_P_STACK();
                Int64 n2 = (Int64)Forth.POP_P_STACK();
                Int64 n3 = (Int64)Forth.POP_P_STACK();

                Int64 n4 = 0;
                Int64 n5 = Math.DivRem(n3 * n2, n1, out n4);

                Forth.PARAM_STACK.Push((Int32)n4);
                Forth.PARAM_STACK.Push((Int32)n5);
            }), true);

        [WordName("POSTPONE")]
        [WordSet(WordSets.CORE)]
        [Depends("WORD")]
        [Depends("FIND")]
        [Depends("LIT")]
        [Depends(",")]
        [Syntax("( \"<spaces>name\" -- )")]
        internal static Lazy<DictionaryEntry> POSTPONE = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("POSTPONE", Forth.FORTH_IMMED_MASK, delegate
              {
                  WORD.Value.Invoke();
                  FIND.Value.Invoke();

                  Int32 adr = Forth.PARAM_STACK.Pop();

                  if (Forth.A2I(adr) > 0)
                  {
                      Forth.Dictionary.Last().Definition.Add(Forth.Locate("LIT"));
                      Forth.Dictionary.Last().Definition.Add(adr);
                      Forth.Dictionary.Last().Definition.Add(Forth.Locate(","));
                  }
              }), true);

        [WordName("EVALUATE")]
        [WordSet(WordSets.CORE)]
        [Depends("C@")]
        [Depends("INTERPRET")]
        [Syntax("( i*x c-addr u -- j*x )")]
        internal static Lazy<DictionaryEntry> EVALUATE = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("EVALUATE", 0, delegate
              {
                  Int32 len = Forth.POP_P_STACK();
                  Int32 adr = Forth.POP_P_STACK();

                  if (len != 0)
                  {
                      // Save Input Queue
                      Queue<Char> tmp = new Queue<Char>();
                      while (Forth.input.Count > 0)
                      {
                          tmp.Enqueue(Forth.input.Dequeue());
                      }

                      for (Int32 i = 0; i < len; i++)
                      {
                          Forth.PUSH_P_STACK(adr + i);
                          FETCHBYTE.Value.Invoke();

                          Char ch = (Char)Forth.POP_P_STACK();
                          Forth.input.Enqueue(ch);
                      }
                      Forth.input.Enqueue('\n');

                      INTERPRET.Value.Invoke();

                      // Restore Input Queue
                      while (tmp.Count > 0)
                      {
                          Forth.input.Enqueue(tmp.Dequeue());
                      }
                  }
              }), true);

        /// <summary>
        /// 
        /// </summary>
        [WordName("DOES>")]
        [WordSet(WordSets.CORE)]
        [Depends("DODOES")]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> DOES = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("DOES>", 0, delegate
        {
            if (Forth.STATE == Forth.COMPILE_MODE)
            {
                Forth.Dictionary.Last().Definition.Add(Forth.Locate("DODOES"));
            }
        }), true);

        /// <summary>
        /// CFA == DFA == FIND == >BODY
        /// </summary>
        [WordName(">BODY")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> TOBODY = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(">BODY", 0, delegate
        {
            // CFA == DFA == FIND == >BODY
            Forth.PUSH_P_STACK(Forth.POP_P_STACK());

            Forth.NEXT();
        }), true);

        /// <summary>
        /// 
        /// </summary>
        [WordName(":NONAME")]
        [WordSet(WordSets.CORE)]
        [Depends("DOCOL")]
        [Depends("(CREATE)")]
        [Depends("LIT")]
        [Depends(",")]
        [Depends("LATEST")]
        [Depends("@")]
        [Depends("HIDDEN")]
        [Depends("]")]
        [Depends("EXIT")]
        [Syntax("( C: -- colon-sys) ( S: -- xt )")]
        internal static Lazy<DictionaryEntry> COLON_NONAME = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry(":NONAME", 0, new Int32[]
            {
                Forth.Locate("DOCOL"),

                //! Create/fake zero length string.
                Forth.Locate("LIT"), 0,
                Forth.Locate("LIT"), 0,
                
                //! CREATE the dictionary entry / header
                //Forth.Locate("(CREATE)"),
                Forth.Dictionary.IndexOf(PAREN_CREATE.Value),

                //! Return value.
                Forth.Locate("LATEST"), Forth.Locate("@"),

                //! Append DOCOL (the codeword).
                Forth.Locate("LIT"), Forth.Locate("DOCOL"), Forth.Locate(","),

                //! Make the word hidden
                Forth.Locate("LATEST"), Forth.Locate("@"), Forth.Locate("HIDDEN"),

                //! Go into compile mode.
                Forth.Locate("]"),

                //! Return from the function.
                Forth.Locate("EXIT"),
            }), true);

        /// <summary>
        /// Pictured Numbers.
        /// 
        /// : &lt;# ( -- ) HERE 128 + PNSPTR ! ;
        /// </summary>
        [WordName("<#")]
        [WordSet(WordSets.CORE)]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> LESSNUMBERSIGN = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("<#", 0, delegate
              {
                  //! Reset PICTURE Storage.
                  Forth.PICTURE = String.Empty;
              }), true);

        /// <summary>
        /// Pictured Numbers.
        /// 
        /// : #&gt; ( xd -- c-addr u ) DROP DROP HERE 128 + PNSPTR @ - PNSPTR @ SWAP ;
        /// </summary>
        [WordName("#>")]
        [WordSet(WordSets.CORE)]
        [Syntax("( xd -- c- addr u )")]
        internal static Lazy<DictionaryEntry> NUMBERSIGNGREATER = new Lazy<DictionaryEntry>(
                () => new DictionaryEntry("#>", 0, delegate
                {
                    //Discard 2 cell number on stack.
                    Forth.POP_P_STACK();
                    Forth.POP_P_STACK();

                    Int32 WORD_BUFFER_START = Forth.STRING_BUFFER.Count;

                    foreach (Char ch in Forth.PICTURE)
                    {
                        Forth.STRING_BUFFER.Add(ch);
                    }

                    Forth.PUSH_P_STACK(WORD_BUFFER_START | Forth.MEM_WRD);
                    Forth.PUSH_P_STACK(Forth.STRING_BUFFER.Count - WORD_BUFFER_START);

                }), true);

        /// <summary>
        /// Pictured Numbers.
        /// 
        /// : HOLD ( char -- ) -1 PNSPTR +! PNSPTR @ C! ;
        /// </summary>
        [WordName("HOLD")]
        [WordSet(WordSets.CORE)]
        [Syntax("( char -- )")]
        internal static Lazy<DictionaryEntry> HOLD = new Lazy<DictionaryEntry>(
                  () => new DictionaryEntry("HOLD", 0, delegate
                  {
                      Forth.PICTURE = (Char)Forth.POP_P_STACK() + Forth.PICTURE;
                  }), true);

        /// <summary>
        /// Pictured Numbers.
        /// 
        /// : SIGN ( n -- ) 0&lt; IF 45 HOLD THEN ;
        /// 
        /// <remarks>
        /// As Sign is stored in &lt;# it can be used even if the number is already 0.
        /// </remarks>
        /// </summary>
        [WordName("SIGN")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- )")]
        internal static Lazy<DictionaryEntry> SIGN = new Lazy<DictionaryEntry>(
                 () => new DictionaryEntry("SIGN", 0, delegate
                 {
                     if (Forth.POP_P_STACK() < 0)
                     {
                         Forth.PICTURE = "-" + Forth.PICTURE;
                     }
                 }), true);

        /// <summary>
        /// Pictured Numbers.
        /// 
        /// : # ( ud1 -- ud2 ) BASE @ M/MOD ROT 9 OVER &lt; IF 7 + THEN 48 + HOLD ;
        /// </summary>
        [WordName("#")]
        [WordSet(WordSets.CORE)]
        [Syntax("( ud1 -- ud2 )")]
        internal static Lazy<DictionaryEntry> NUMBERSIGN = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("#", 0, delegate
               {
                   // test with : .DOLLAR <# SIGN # # 46 HOLD #S 36 HOLD #> TYPE ; -59998 S>D -1 .DOLLAR 

                   //Int64 n1 = (((Int64)Forth.POPPSP()) << 32) | (Int64)Forth.POPPSP();  //MSW, LSW
                   Int64 n1 = Forth.POP_P_STACK_2D();

                   Int64 n2;
                   Int64 n3 = Math.DivRem(n1, Forth.BASE, out n2);
                   n2 = Math.Abs(n2);
                   Forth.PICTURE = (n2 < 10 ? (Char)(n2 + '0') : (Char)((n2 - 10) + 'A')) + Forth.PICTURE;

                   //Forth.PUSH_P_STACK((Int32)(n3 & 0xffffffff));     //LSW
                   //Forth.PUSH_P_STACK((Int32)(n3 >> 32));            //MSW
                   Forth.PUSH_D2P_STACK(n3);
               }), true);

        /// <summary>
        /// : #S ( ud1 -- ud2 ) BEGIN # OVER OVER OR 0= UNTIL ;
        /// </summary>
        [WordName("#S")]
        [WordSet(WordSets.CORE)]
        [Syntax("( ud1 -- ud2 )")]
        internal static Lazy<DictionaryEntry> NUMBERSIGNS = new Lazy<DictionaryEntry>(
                () => new DictionaryEntry("#S", 0, delegate
                {
                    Int64 n1 = Forth.POP_P_STACK_2D();  //MSW, LSW
                    Int64 n3;

                    while (n1 != 0)
                    {
                        n1 = Math.DivRem(Math.Abs(n1), Forth.BASE, out n3);
                        n3 = Math.Abs(n3);
                        Forth.PICTURE = (n3 < 10 ? (Char)(n3 + '0') : (Char)((n3 - 10) + 'A')) + Forth.PICTURE;
                    }

                    Forth.PUSH_P_STACK((Int32)(n1 & 0xffffffff));    //LSW
                    Forth.PUSH_P_STACK((Int32)(n1 >> 32));           //MSW
                }), true);

        /// <summary>
        /// Convert Single Cell number to a Double Cell one.
        /// </summary>
        [WordName("S>D")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n -- d )")]
        internal static Lazy<DictionaryEntry> STOD = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("S>D", 0, delegate
               {
                   Int64 n1 = (Int64)Forth.POP_P_STACK();

                   Forth.PUSH_P_STACK((Int32)(n1 & 0xffffffff));     //LSW
                   Forth.PUSH_P_STACK((Int32)(n1 >> 32));            //MSW

               }), true);

        /// <summary>
        /// Multiply n1 by n2 and store a signed double-cell product ud.
        /// </summary>
        [WordName("M*")]
        [WordSet(WordSets.CORE)]
        [Syntax("( n1 n2 -- d )")]
        internal static Lazy<DictionaryEntry> M_STAR = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("M*", 0, delegate
               {
                   Int64 n2 = (Int64)Forth.POP_P_STACK();
                   Int64 n1 = (Int64)Forth.POP_P_STACK();

                   Forth.PUSH_D2P_STACK(n1 * n2);
               }), true);

        /// <summary>
        /// Multiply u1 by u2 and store a unsigned double-cell productr ud.
        /// </summary>
        [WordName("UM*")]
        [WordSet(WordSets.CORE)]
        [Syntax("( u1 u2 -- ud )")]
        internal static Lazy<DictionaryEntry> UM_STAR = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("UM*", 0, delegate
               {
                   UInt64 n1 = (UInt64)Forth.POP_P_STACK_2U();
                   UInt64 n2 = (UInt64)Forth.POP_P_STACK_2U();

                   Forth.PUSH_UD2P_STACK(n1 * n2);
               }), true);

        /// <summary>
        /// Floored Quotient and Remainder.
        /// </summary>
        [WordName("FM/MOD")]
        [WordSet(WordSets.CORE)]
        [Syntax("( d1 n1 -- n2 n3 )")]
        internal static Lazy<DictionaryEntry> FM_SLASH_MOD = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("FM/MOD", 0, delegate
               {
                   Int32 n1 = Forth.POP_P_STACK();      // b
                   Int64 d1 = Forth.POP_P_STACK_2D();   // a

                   Int64 n3 = (Int64)Math.Floor(d1 / (1.0 * n1));
                   Int64 n2 = d1 - (n1 * n3);

                   Forth.PUSH_P_STACK((Int32)n2);   // Remainder
                   Forth.PUSH_P_STACK((Int32)n3);   // Floored Quotient
               }), true);

        /// <summary>
        /// Floored Quotient and Remainder.
        /// </summary>
        [WordName("UM/MOD")]
        [WordSet(WordSets.CORE)]
        [Syntax("( ud1 u1 -- u2 u3 )")]
        internal static Lazy<DictionaryEntry> UM_SLASH_MOD = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("UM/MOD", 0, delegate
               {
                   UInt32 n1 = (UInt32)Forth.POP_P_STACK();      // b
                   UInt64 d1 = Forth.POP_P_STACK_2UD();   // a

                   UInt64 n3 = (d1 / n1);
                   UInt64 n2 = d1 - (n1 * n3);

                   Forth.PUSH_P_STACK((Int32)(UInt32)n2);   // Remainder
                   Forth.PUSH_P_STACK((Int32)(UInt32)n3);   // Quotient
               }), true);

        /// <summary>
        /// Symmetric Quotient and Remainder.
        /// </summary>
        [WordName("SM/REM")]
        [WordSet(WordSets.CORE)]
        [Syntax("( d1 n1 -- n2 n3 )")]
        internal static Lazy<DictionaryEntry> SM_SLASH_REM = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("SM/REM", 0, delegate
               {
                   Int32 n1 = Forth.POP_P_STACK();      // b
                   Int64 d1 = Forth.POP_P_STACK_2D();   // a

                   Int64 n2;
                   Int64 n3 = Math.DivRem(d1, n1, out n2);
                   //Int64 n2 = d1 - (n1 * n3);

                   Forth.PUSH_P_STACK((Int32)n2);   // Remainder
                   Forth.PUSH_P_STACK((Int32)n3);   // Quotient
               }), true);

        /// <summary>
        /// Environment Queries.
        /// </summary>
        [WordName("ENVIRONMENT?")]
        [WordSet(WordSets.CORE)]
        [Syntax("( c-addr u -- false | i*x true )")]
        internal static Lazy<DictionaryEntry> ENVIRONMENT_QUERY = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("ENVIRONMENT?", 0, delegate
               {
                   Int32 length = Forth.POP_P_STACK();
                   Int32 start = Forth.POP_P_STACK();

                   String query = Forth.FetchWord(start, length, true);

                   //! Buffer Sizes.

                   if (query.Equals("/COUNTED-STRING"))
                   {
                       Forth.PUSH_P_STACK(254);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("/HOLD"))
                   {
                       //! Minimum is 2*n + 2 where n is the size of a cell in bits.
                       Forth.PUSH_P_STACK(256);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("/PAD"))
                   {
                       Forth.PUSH_P_STACK(Forth.PAD_SIZE);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }

                    //! Address Units.

                   else if (query.Equals("ADDRESS-UNITS-BITS"))
                   {
                       Forth.PUSH_P_STACK(8);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }

                   //! Math.

                   else if (query.Equals("FLOORED"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }

                   //! Floating.

                   else if (query.Equals("FLOATING-STACK"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("MAX-FLOAT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                       Forth.PUSH_F_STACK(Double.MaxValue);
                   }

                   //! Numbers.

                   else if (query.Equals("MAX_CHAR"))
                   {
                       Forth.PUSH_P_STACK(127);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("MAX-D"))
                   {
                       Forth.PUSH_UD2P_STACK(UInt64.MaxValue);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("MAX-N"))
                   {
                       Forth.PUSH_P_STACK(Int32.MaxValue);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("MAX-U"))
                   {
                       Forth.PUSH_U2P_STACK(UInt32.MaxValue);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("MAX-UD"))
                   {
                       Forth.PUSH_UD2P_STACK(UInt32.MaxValue);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }

                   //! Stacks.

                   else if (query.Equals("RETURN-STACK-CELLS"))
                   {
                       Forth.PUSH_P_STACK(65536);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("STACK-CELLS"))
                   {
                       Forth.PUSH_P_STACK(65536);
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }

                   //! Locals.

                   else if (query.Equals("#LOCALS"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }

                   //! Locals.

                   else if (query.Equals("#LOCALS"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }

                   //! Search-order.

                   else if (query.Equals("WORDLISTS"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }

                   //! Supported Words Sets!

                   else if (query.Equals("CORE") || query.Equals("CORE-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("BLOCK") || query.Equals("BLOCK-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("DOUBLE"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("DOUBLE-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("EXCEPTION") || query.Equals("EXCEPTION-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("FACILITY"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("FACILITY-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("FILE") || query.Equals("FILE-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("FLOATING") || query.Equals("FLOATING-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("LOCALS") || query.Equals("LOCALS-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("MEMORY") || query.Equals("MEMORY-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("TOOLS"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_TRUE);
                   }
                   else if (query.Equals("TOOLS-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("SEARCH-ORDER") || query.Equals("SEARCH-ORDER-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }
                   else if (query.Equals("STRING") || query.Equals("STRING-EXT"))
                   {
                       Forth.PUSH_P_STACK(Forth.FORTH_FALSE);
                   }

               }), true);

        /// <summary>
        /// 
        /// </summary>
        [WordName("ABORT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( i*x -- ) ( R: j*x -- ) ( F: k*r -- )")]
        internal static Lazy<DictionaryEntry> ABORT = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("ABORT", 0, delegate
               {
                   throw new AbortException(-1);
               }), true);

        /// <summary>
        /// 
        /// </summary>
        [WordName("(ABORT\")")]
        [WordSet(WordSets.CORE)]
        [Syntax("( i*x x1 -- | i*x ) ( R: j*x -- | j*x ) ( F: k*r -- | k*x )")]
        internal static Lazy<DictionaryEntry> PAREN_ABORT_QUOTE = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("(ABORT\")", Forth.FORTH_HIDDEN_MASK, delegate
               {
                   //! Handled in DictionaryEntry.Execute.
               }), true);

        /// <summary>
        /// 
        /// </summary>
        [WordName("ABORT\"")]
        [WordSet(WordSets.CORE)]
        [Syntax("( i*x x1 -- | i*x ) ( R: j*x -- | j*x ) ( F: k*r -- | k*x )")]
        internal static Lazy<DictionaryEntry> ABORT_QUOTE = new Lazy<DictionaryEntry>(
               () => new DictionaryEntry("ABORT\"", Forth.FORTH_IMMED_MASK, delegate
               {
                   switch (Forth.STATE)
                   {
                       case Forth.COMPILE_MODE:
                           {
                               Forth.Dictionary[Forth.Dictionary.Count - 1].Definition.Add(Forth.Locate("(ABORT\")"));

                               //! Save Length Location so we can update.
                               Int32 ndx = Forth.Dictionary.Last().Definition.Count;
                               Forth.Dictionary.Last().Definition.Add(0);

                               Char key = (Char)0;

                               Int32ByteUnion val = new Int32ByteUnion(0);

                               Int32 len = 0;
                               do
                               {
                                   key = Forth.DoInputChar();

                                   if (key != '\"')
                                   {
                                       val[len % 4] = (byte)key;

                                       if (len % 4 == 3)
                                       {
                                           Forth.Dictionary.Last().Definition.Add(val.iVal);
                                           val.iVal = 0;
                                       }

                                       len++;
                                   }

                               } while (key != '\"');

                               if (len % 4 != 0)
                               {
                                   Forth.Dictionary.Last().Definition.Add(val.iVal);
                                   val.iVal = 0;
                               }

                               Forth.Dictionary.Last().Definition[ndx] = len;
                           }
                           break;

                       case Forth.IMMEDIATE_MODE:
                           {
                               //! The following works 
                               Int32 flag = Forth.POP_P_STACK();

                               String msg = String.Empty;
                               Char key;
                               do
                               {
                                   key = Forth.DoInputChar();

                                   if (key != '\"')
                                   {
                                       msg += key;
                                   }

                               } while (key != '\"');

                               if (flag != 0)
                               {
                                   //! TODO Dump Return Stack and add it.
                                   throw new AbortException(-2, msg);
                               }
                           }
                           break;
                   }

               }), true);

        #endregion Forth Core Words.

        #region Forth Core Ext Words.

        /// <summary>
        /// 1146 (n1 n2 -- 1 if n1 != n2 | 0 if n1 == n2)
        /// </summary>
        [WordName("<>")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( x1 x2 -- flag )")]
        internal static Lazy<DictionaryEntry> NEQ = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("<>", 0, delegate
             {
                 Int32 n2 = Forth.POP_P_STACK();
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push(n1 != n2 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1180 (n1 -- 1 if n1 != 0 | 0 if n1 == 0)
        /// </summary>
        [WordName("0<>")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( x -- flag )")]
        internal static Lazy<DictionaryEntry> ZNEQU = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("0<>", 0, delegate
             {
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push(n1 != 0 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1188 (n1 -- 1 if n1 &gt; 0 | 0 if n1 &lt;= 0)
        /// </summary>
        [WordName("0>")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( n-- flag )")]
        internal static Lazy<DictionaryEntry> ZGT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("0>", 0, delegate
             {
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push(n1 > 0 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                 Forth.NEXT();
             }), true);

        [WordName("VALUE")]
        [WordSet(WordSets.CORE_EXT)]
        [Depends("CREATE")]
        [Depends("DOCOL")]
        [Depends("LIT")]
        [Depends("EXIT")]
        [Syntax("( z \"<spaces>name\" -- )")]
        internal static Lazy<DictionaryEntry> VALUE = new Lazy<DictionaryEntry>(
                () => new DictionaryEntry("VALUE", 0, delegate
                {
                    CREATE.Value.Invoke();

                    Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(Forth.Locate("DOCOL"));

                    Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(Forth.Locate("LIT"));

                    //! Initial Value
                    Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(Forth.POP_P_STACK());

                    Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(Forth.Locate("EXIT"));
                }), true);

        [WordName("TO")]
        [WordSet(WordSets.CORE_EXT)]
        [Depends("WORD")]
        [Depends("FIND")]
        [Syntax("( \"<spaces>name\" -- )")]
        internal static Lazy<DictionaryEntry> TO = new Lazy<DictionaryEntry>(
                () => new DictionaryEntry("TO", Forth.FORTH_IMMED_MASK, delegate
                {
                    WORD.Value.Invoke();
                    FIND.Value.Invoke();

                    Int32 adr = Forth.POP_P_STACK();

                    switch (Forth.STATE)
                    {
                        case Forth.COMPILE_MODE:
                            {
                                //! LIT
                                Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(Forth.Locate("LIT"));

                                //! Address inside VALUE
                                Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(adr + (2 << 2));

                                //! Store PSP's Top there.
                                Forth.Dictionary[Forth.A2I(Forth.LATEST)].Definition.Add(Forth.Locate("!"));
                            }
                            break;
                        case Forth.IMMEDIATE_MODE:
                            {
                                Forth.Dictionary[Forth.A2I(adr)].Definition[2] = Forth.POP_P_STACK();
                            }
                            break;
                    }
                }), true);

        /// <summary>
        /// bb4wForth Unsigned GT.
        /// </summary>
        [WordName("U>")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("(n1 n2 -- flag )")]
        internal static Lazy<DictionaryEntry> UGT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("U>", 0, delegate
             {
                 Int32 n2 = Forth.POP_P_STACK();
                 Int32 n1 = Forth.POP_P_STACK();

                 Forth.PARAM_STACK.Push((UInt32)n1 > (UInt32)n2 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);

                 Forth.NEXT();
             }), true);

        [WordName("PICK")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( xu ... x1 x0 u -- xu ... x1 x0 xu )")]
        internal static Lazy<DictionaryEntry> PICK = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("PICK", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();
                Int32 n2 = Forth.PARAM_STACK.ToArray()[n1];
                Forth.PUSH_P_STACK(n2);
            }), true);

        /// <summary>
        /// 
        /// </summary>
        [WordName("ROLL")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( xu ... x1 x0 u -- xu-1 ... x1 x0 xu )")]
        internal static Lazy<DictionaryEntry> ROLL = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("ROLL", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();

                Int32[] stack = Forth.PARAM_STACK.ToArray();
                Int32 n2 = stack[n1];

                Forth.PARAM_STACK.Clear();
                for (Int32 i = (stack.Count() - 1); i >= 0; i--)
                {
                    if (i != n1)
                    {
                        Forth.PUSH_P_STACK(stack[i]);
                    }
                }

                Forth.PUSH_P_STACK(n2);
            }), true);

        /// <summary>
        /// Store the input queue onto the PARAM_STACK.
        /// <remarks>
        /// We make sure that we only save the characters 
        /// present at entry.
        /// </remarks>
        /// </summary>
        [WordName("SAVE-INPUT")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( -- xn ... x1 n )")]
        internal static Lazy<DictionaryEntry> SAVE_INPUT = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("SAVE-INPUT", 0, delegate
            {
                Int32 n = 0;

                //! Note we need to do it reversed so we can Enqueu in RESTORE-INPUT.
                foreach (Char ch in Forth.input.ToArray().Reverse())
                {
                    Forth.PUSH_P_STACK(ch);
                    n++;
                }

                //! Remove only the characters stored.
                for (Int32 i = 0; i < n; i++)
                {
                    Forth.input.Dequeue();
                }

                //! Character Count.
                Forth.PUSH_P_STACK(n);
            }), true);

        /// <summary>
        /// Restore the input queue from the PARAM_STACK.
        /// </summary>
        [WordName("RESTORE-INPUT")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( xn ... x1 n -- flag )")]
        internal static Lazy<DictionaryEntry> RESTORE_INPUT = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("RESTORE-INPUT", 0, delegate
            {
                //! Character Count.
                Int32 n = Forth.POP_P_STACK();

                //! Restore Characters .
                while (n > 0 && Forth.PARAM_STACK.Count > 0)
                {
                    Forth.input.Enqueue((Char)Forth.POP_P_STACK());
                    n--;
                }

                //! Push Result Flag.
                Forth.PUSH_P_STACK(n == 0 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);
            }), true);

        /// <summary>
        /// Accept up to n1 characters (and terminate at cr or lf).
        /// 
        /// <remarks>
        /// Due to it's nature ACCEPT is not buffered and will 
        /// not provide Console editing funtionality.
        /// </remarks>
        /// </summary>
        [WordName("ACCEPT")]
        [WordSet(WordSets.CORE)]
        [Syntax("( c-addr +n1 -- +n2 )")]
        internal static Lazy<DictionaryEntry> ACCEPT = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("ACCEPT", 0, delegate
            {
                Int32 n1 = Forth.POP_P_STACK();
                Int32 adr = Forth.POP_P_STACK();
                Int32 n2 = 0;

                do
                {
                    Char dat = Forth.DoInputChar(false);
                    if (dat == '\0')
                    {
                        continue;
                    }

                    if (dat == '\n' || dat == '\r')
                    {
                        break;
                    }

                    switch (Forth.A2M(adr))
                    {
                        case Forth.MEM_DEF:
                            Int32ByteUnion val = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)]);
                            val[adr % 4] = (byte)dat;
                            Forth.Dictionary[Forth.A2I(adr)].Definition[Forth.A2D(adr)] = val.iVal;
                            break;
                        case Forth.MEM_RSP:
                            Forth.DoOutput("MemoryType 1 Not Implemented");
                            break;
                        case Forth.MEM_PRM:
                            Forth.DoOutput("MemoryType 2 Not Implemented");
                            break;
                        case Forth.MEM_WRD:
                            Forth.STRING_BUFFER[Forth.A2A(adr)] = (Char)dat;
                            break;
                    }
                    adr++;
                    n2++;
                } while (n2 < n1);

                Forth.PUSH_P_STACK(n2);
            }), true);

        /// <summary>
        /// Refill Input Queue.
        /// </summary>
        [WordName("REFILL")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( -- flag )")]
        internal static Lazy<DictionaryEntry> REFILL = new Lazy<DictionaryEntry>(
            () => new DictionaryEntry("REFILL", 0, delegate
            {
                Forth.PUSH_P_STACK(Forth.input.Count > 0 ? Forth.FORTH_TRUE : Forth.FORTH_FALSE);
            }), true);

        /// <summary>
        /// Length of String received by EXPECT.
        /// </summary>
        [WordName("SPAN")]
        [WordSet(WordSets.CORE_EXT)]
        [Depends("DOCOL")]
        [Depends("LIT")]
        [Depends("BRANCH")]
        [Depends("EXIT")]
        [Syntax("( -- addr )")]
        internal static Lazy<DictionaryEntry> SPAN = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("SPAN", 0, new Int32[]
                {
                    Forth.Locate("DOCOL"),

                    Forth.Locate("LIT"),
                    ((Forth.Dictionary.IndexOf(Forth.Dictionary.Last()) + 1) << 16) + 24,
                    
                    Forth.Locate("BRANCH"), // Jump over LATEST
                    12,
                    //
                    Forth.Locate("LIT"),
                    Forth.SPAN,
                    //
                    Forth.Locate("EXIT")
                }), true);

        /// <summary>
        /// Expect a number of characters.
        /// <remarks>
        /// Like ACCEPT but actual number of receive characters is stored in SPAN.
        /// </remarks>
        /// </summary>
        [WordName("EXPECT")]
        [WordSet(WordSets.CORE_EXT)]
        [Depends("ACCEPT")]
        [Syntax("( c-addr +n -- )")]
        internal static Lazy<DictionaryEntry> EXPECT = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("EXPECT", 0, delegate
            {
                Forth.SPAN = 0;

                Forth.Dictionary[Forth.LocateI("ACCEPT")].Invoke();

                Forth.SPAN = Forth.POP_P_STACK();
            }), true);

        /// <summary>
        /// 3200 - Standard words for booleans
        /// </summary>
        [WordName("TRUE")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( -- true )")]
        internal static String TRUE = ": TRUE -1 ;\n";

        /// <summary>
        /// 3200 - Standard words for booleans
        /// </summary>
        [WordName("FALSE")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( -- false )")]
        internal static String FALSE = ": FALSE 0 ;\n";

        /// <summary>
        /// Input Source Id.
        /// <remarks>
        /// Fixed to User Input Device for now.
        /// </remarks>
        /// </summary>
        [WordName("SOURCE-ID")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( -- 0|-1 )")]
        internal static Lazy<DictionaryEntry> SOURCE_ID = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("SOURCE-ID", 0, delegate
             {
                 Forth.PUSH_P_STACK(0);
             }), true);


        /// <summary>
        /// Parses the Input up to a delimiter.
        /// </summary>
        [WordName("PARSE")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( -- 0|-1 )")]
        internal static Lazy<DictionaryEntry> PARSE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("PARSE", 0, delegate
             {
                 Int32 WORD_BUFFER_START = Forth.STRING_BUFFER.Count;

                 if (Forth.DoInputAvailable())
                 {
                     Char delim = Forth.DoInputChar();

                     Char key;
                     do
                     {
                         key = Forth.DoInputChar();
                         if (key != delim)
                         {
                             Forth.STRING_BUFFER.Add(key);
                         }
                     } while (key != delim);
                 }

                 Forth.PARAM_STACK.Push(WORD_BUFFER_START | Forth.MEM_WRD);
                 Forth.PARAM_STACK.Push(Forth.STRING_BUFFER.Count - WORD_BUFFER_START);

                 Forth.NEXT();
             }), true);


        /// <summary>
        /// MARKER creates a word that when executed will forget itself anf every word defined after it.
        /// </summary>
        [WordName("MARKER")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( \"<spaces>name\" -- )")]
        internal static Lazy<DictionaryEntry> MARKER = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("MARKER", 0, delegate
             {
                 CREATE.Value.Invoke();

                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("DOCOL"));
                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("LIT"));
                 Forth.Dictionary.Last().Definition.Add(Forth.LATEST);
                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("(MARKER)"));
                 Forth.Dictionary.Last().Definition.Add(Forth.Locate("EXIT"));

             }), true);

        /// <summary>
        /// Helper for MARKER, the actual forget is called here.
        /// </summary>
        [WordName("(MARKER)")]
        [WordSet(WordSets.CORE_EXT)]
        [Syntax("( xt -- )")]
        internal static Lazy<DictionaryEntry> PAREN_MARKER = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("(MARKER)", Forth.FORTH_HIDDEN_MASK, delegate
             {
                 Forth.ForgetMarker(Forth.A2I(Forth.POP_P_STACK()));
             }), true);
        #endregion Forth Core Ext Words.
    }
}
