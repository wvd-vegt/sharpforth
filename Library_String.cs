﻿// -----------------------------------------------------------------------
// <copyright file="Library_Exception.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------
namespace SharpForth
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;

    /// <summary>
    /// This class contains the definitions of some additional pure Forth Words.
    /// </summary>
    public static partial class Library
    {
        #region Forth Exception Words.

        /// <summary>
        /// Fill Memory with zero's.
        /// </summary>
        [WordName("BLANK")]
        [WordSet(WordSets.STRING)]
        [Depends("(")]
        [Depends("FILL")]
        [Depends("BL")]
        [Syntax("( c-addr u -- )")]
        internal static String BLANK = ": BLANK ( c-addr u -- ) BL FILL ;\n";

        #endregion Forth Exception Words.
    }
}