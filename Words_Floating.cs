﻿// -----------------------------------------------------------------------
// <copyright file="Words_Floating.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// This static class contains all built-in Forth Words.
    /// </summary>
    public static partial class Words
    {
        #region Forth Floating Point Words

#warning todo Float only when BASE is decimal.
#warning todo Correct Formating Mask (+1234.5678E-6).
#warning todo Parse numbers to Float when E is present (E is mandatory!).

        //[WordName("FP!")]
        //[Depends("!")]
        //[Syntax("( fp adr -- )")]
        //internal static Lazy<DictionaryEntry> FPSTORE = new Lazy<DictionaryEntry>(
        //      () => new DictionaryEntry("FP!", 0, delegate
        //      {
        //          Double fp = Forth.POPFPSP();
        //          Int32 adr = Forth.POPPSP();

        //          Forth.Dictionary[Forth.LocateI("!")].Invoke();
        //      }), true);

        [WordName("/F")]
        [WordSet(WordSets.SHARPFORTH)]
        [Depends("XYYZ")]
        [Syntax("( -- n )")]
        internal static Lazy<DictionaryEntry> SLASHF = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("/F", 0, delegate
              {
                  Forth.PUSH_P_STACK(sizeof(Double));
              }), true);

        [WordName("FDEPTH")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( -- n )")]
        internal static Lazy<DictionaryEntry> FDEPTH = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("FDEPTH", 0, delegate
              {
                  Forth.PUSH_P_STACK(Forth.FPARAM_STACK.Count);
              }), true);

        [WordName("FCLEAR")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( -- )")]
        internal static Lazy<DictionaryEntry> FCLEAR = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("FCLEAR", 0, delegate
              {
                  Forth.FPARAM_STACK.Clear();
              }), true);

        [WordName("FLOAT")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( n -- fp )")]
        internal static Lazy<DictionaryEntry> FLOAT = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("FLOAT", 0, delegate
              {
                  Forth.PUSH_F_STACK(((Double)Forth.POP_P_STACK()));
              }), true);

        [WordName("FIX")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( fp -- n )")]
        internal static Lazy<DictionaryEntry> FIX = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("FIX", 0, delegate
              {
                  Forth.PUSH_P_STACK((Int32)Math.Round(Forth.POP_F_STACK()));

                  //! Exception on overflow, return 0.
              }), true);

        [WordName("INT")]
        [WordSet(WordSets.SHARPFORTH)]
        [Syntax("( fp -- n )")]
        internal static Lazy<DictionaryEntry> INT = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("INT", 0, delegate
              {
                  Forth.PUSH_P_STACK((Int32)Math.Floor(Forth.POP_F_STACK()));

                  //! Exception on overflow, return 0.
              }), true);

        [WordName("F.")]
        [WordSet(WordSets.FLOATING)]
        [Syntax("( F: r -- )")]
        internal static Lazy<DictionaryEntry> FPDOT = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("F.", 0, delegate
              {
                  Forth.DoOutput(String.Format("{0} ", Forth.POP_F_STACK().ToString("F")));
              }), true);

        [WordName("F+")]
        [WordSet(WordSets.FLOATING)]
        [Syntax("( F: r1 r2 -- r3r )")]
        internal static Lazy<DictionaryEntry> FPPLUS = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("F+", 0, delegate
              {
                  Double fp2 = Forth.POP_F_STACK();
                  Double fp1 = Forth.POP_F_STACK();

                  Forth.PUSH_F_STACK(fp1 + fp2);
              }), true);

        [WordName("F-")]
        [WordSet(WordSets.FLOATING)]
        [Syntax("( F: r1 r2 -- r3 )")]
        internal static Lazy<DictionaryEntry> FPMINUS = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("F-", 0, delegate
              {
                  Double fp2 = Forth.POP_F_STACK();
                  Double fp1 = Forth.POP_F_STACK();

                  Forth.PUSH_F_STACK(fp1 - fp2);
              }), true);

        [WordName("F*")]
        [WordSet(WordSets.FLOATING)]
        [Syntax("( F: r1 r2 -- r3r )")]
        internal static Lazy<DictionaryEntry> FPMUL = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("F*", 0, delegate
              {
                  Double fp2 = Forth.POP_F_STACK();
                  Double fp1 = Forth.POP_F_STACK();

                  Forth.PUSH_F_STACK(fp1 * fp2);
              }), true);

        [WordName("F/")]
        [WordSet(WordSets.FLOATING)]
        [Syntax("( F: r1 r2 -- r3r )")]
        internal static Lazy<DictionaryEntry> FPDIV = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("F/", 0, delegate
              {
                  Double fp2 = Forth.POP_F_STACK();
                  Double fp1 = Forth.POP_F_STACK();

                  Forth.PUSH_F_STACK(fp1 / fp2);
              }), true);

        [WordName("F**")]
        [WordSet(WordSets.FLOATING_EXT)]
        [Syntax("( F: r1 r2 -- r3r )")]
        internal static Lazy<DictionaryEntry> FPPOWER = new Lazy<DictionaryEntry>(
              () => new DictionaryEntry("F**", 0, delegate
              {
                  Double fp2 = Forth.POP_F_STACK();
                  Double fp1 = Forth.POP_F_STACK();

                  Forth.PUSH_F_STACK(Math.Pow(fp1, fp2));
              }), true);

        #endregion Forth Floating Point Words
    }
}
