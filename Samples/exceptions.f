: FOO ( n -- ) THROW ;

: TEST-EXCEPTIONS
    25 ['] FOO CATCH
    ?DUP IF
         ." called FOO and it threw exception number: "
         . CR
           DROP
        THEN
;

TEST-EXCEPTIONS
