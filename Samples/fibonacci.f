﻿\ Fibonacci 

\ http://forthfreak.net/index.cgi?Fibonacci

: fib ( n -- fib )
  0 1 ROT 0 ?DO  OVER + SWAP LOOP DROP 
;

22 CONSTANT maxN

: fibN ( -- )
  maxN 0 DO I fib . LOOP 
;

CR ." The Fibionacci numbers 0 .. " maxN . ." are " CR

fibN