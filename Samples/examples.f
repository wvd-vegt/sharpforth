﻿: GCD ( a b -- gcd ) 
  ?DUP IF 
    TUCK
    MOD
    RECURSE
  THEN
;

\ least common denominator
: LCD ( u1 u2 -- u3 )
      2DUP GCD */ ;

\ Should print 16 OK
784 48 GCD ." Greates Common Divider of 748 and 48 is " . CR

\ Should print 21 OK
3 7 LCD  ." Least Common Denominator of 3 and 7 is " . CR

\ origin: http://c2.com/cgi/wiki?ExampleForthCode
\
\ Note 1: Text after a backslash is a comment until end-of-line.
\ Note 2: Text within parentheses like "( n -- )" is also a comment.
\ Note 3: To be strictly ANSI compliant, the code below is in UPPERCASE.
\	 Most PC Forth implementations are (optionally) case insensitive.



 : STAR	 ( -- )            \ Print a single star
   42 EMIT ;	           \ 42 is the ASCII code for *



 : STARS	( n -- )   \ Print n stars
   0 DO STAR LOOP ;	   \ Loop n times (0 up to n-1) and execute STAR



 : SQUARE	( n -- )   \ Print an n-line square of stars
   DUP 0 DO		   \ Loop n times, keeping (DUP-licating) n on the stack
   DUP STARS CR            \ Each time, print n stars then print CR
   LOOP DROP ;             \ After loop is done, drop the n from the stack



 : TRIANGLE	( n -- )   \ Print an n-line triangle
   1 + 1 DO		   \ Loop n times from 1 to n (instead of 0 to n-1)
   I STARS CR              \ This time use the inner loop index I
   LOOP ;



 : TOWER	( n -- )   \ Print a "tower" with an base of size n
   DUP                     \ DUP-licate n (since it is used twice below)
   1 - TRIANGLE            \ Print a triangle 1 size smaller than n
   SQUARE ;                \ Print a square base of size n

CR ." 7 STARS:" CR
CR 7 STARS
CR
CR ." 4 SQUARE:" CR
CR 4 SQUARE
CR
CR ." 3 TRIANGLE:" CR
CR 3 TRIANGLE
CR
CR ." 6 TOWER:" CR
CR 6 TOWER