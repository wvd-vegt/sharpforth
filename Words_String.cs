﻿// -----------------------------------------------------------------------
// <copyright file="Words_String.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// This static class contains all built-in Forth Words.
    /// </summary>
    public static partial class Words
    {
        #region String Forth Words

        /// <summary>
        /// 1364 Moves a Byte Range. 
        /// 
        /// Note CMOVE is not allowed to cross word Definition Boundaries.
        /// </summary>
        [WordName("CMOVE")]
        [WordSet(WordSets.STRING)]
        [Syntax("( c-addr1 c-addr2 u -- )")]
        internal static Lazy<DictionaryEntry> C_MOVE = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("CMOVE", 0, delegate
             {
                 Int32 cnt = Forth.POP_P_STACK();
                 Int32 dst = Forth.POP_P_STACK();
                 Int32 src = Forth.POP_P_STACK();

                 byte dat = 0;

                 for (Int32 i = 0; i < cnt; i++)
                 {
                     Int32 adr1 = src + i;
                     switch (Forth.A2M(adr1))
                     {
                         case Forth.MEM_DEF:
                             dat = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr1)].Definition[Forth.A2D(adr1)])[adr1 % 4];
                             break;
                         case Forth.MEM_RSP:
                             Forth.DoOutput("MemoryType 1 Not Implemented");
                             return;
                         case Forth.MEM_PRM:
                             Forth.DoOutput("MemoryType 2 Not Implemented");
                             return;
                         case Forth.MEM_WRD:
                             dat = (Byte)Forth.STRING_BUFFER[Forth.A2A(adr1)];
                             return;
                     }

                     Int32 adr2 = dst + i;
                     switch (Forth.A2M(adr2))
                     {
                         case Forth.MEM_DEF:
                             Int32ByteUnion val = new Int32ByteUnion(Forth.Dictionary[Forth.A2I(adr2)].Definition[Forth.A2D(adr2)]);
                             val[adr2 % 4] = (byte)dat;
                             Forth.Dictionary[Forth.A2I(adr2)].Definition[Forth.A2D(adr2)] = val.iVal;
                             break;
                         case Forth.MEM_RSP:
                             Forth.DoOutput("MemoryType 1 Not Implemented");
                             return;
                         case Forth.MEM_PRM:
                             Forth.DoOutput("MemoryType 2 Not Implemented");
                             return;
                         case Forth.MEM_WRD:
                             Forth.STRING_BUFFER[Forth.A2A(adr2)] = (Char)adr2;
                             return;
                     }
                 }

                 Forth.NEXT();
             }), true);

        /// <summary>
        /// 1364 Moves a Byte Range. 
        /// 
        /// Note CMOVE is not allowed to cross word Definition Boundaries.
        /// </summary>
        [WordName("CMOVE>")]
        [WordSet(WordSets.STRING)]
        [Depends("CMOVE")]
        [Syntax("( c-addr1 c-addr2 u -- )")]
        internal static Lazy<DictionaryEntry> C_MOVE_UP = new Lazy<DictionaryEntry>(
             () => new DictionaryEntry("CMOVE>", 0, delegate
             {
                 Int32 cnt = Forth.POP_P_STACK();

                 if (cnt > 0)
                 {
                     Forth.PUSH_P_STACK(cnt);
                     C_MOVE.Value.Invoke();
                 }
                 else
                 {
                     //! Clean Stack, Pop addresses.
                     Forth.POP_P_STACK();
                     Forth.POP_P_STACK();
                 }
                 
                 Forth.NEXT();
             }), true);

        #endregion String Forth Words
    }
}
