﻿\ Clear stack from load-file result.

EMPTY-STACK

\ ------------------------------------------------------------------------
CR ." Extra - testing numeric limits"
\ ------------------------------------------------------------------------

DECIMAL
 4294967295                     CONSTANT max-uint
 2147483647                     CONSTANT max-int
-2147483648                     CONSTANT min-int
HEX
7FFFFFFF                        CONSTANT mid-uint
80000000                        CONSTANT mid-uint+1
DECIMAL

\ Test above against bb4wForths's definition code.

.{ 0 INVERT                 -> max-uint   }
.{ 0 INVERT 1 RSHIFT        -> max-int    }
.{ 0 INVERT 1 RSHIFT INVERT -> min-int    }
.{ 0 INVERT 1 RSHIFT        -> mid-uint   }
.{ 0 INVERT 1 RSHIFT INVERT -> mid-uint+1 }

\ These differ from bb4wForth's definition based on 0 and 0 INVERT.

FALSE CONSTANT <false>												\  0    
TRUE  CONSTANT <true>												\ -1

: T/MOD  >R S>D R> SM/REM ;	\>R & R> park the TOS out of the way. ROT ROT twice shouls also work
: T/     T/MOD SWAP DROP ;
: TMOD   T/MOD DROP ;
: T*/MOD >R M* R> SM/REM ;
: T*/    T*/MOD SWAP DROP ;

{ MAX-INT 2 MAX-INT */ -> MAX-INT 2 MAX-INT T*/ }
{ MIN-INT 2 MIN-INT */ -> MIN-INT 2 MIN-INT T*/ }
{ MAX-INT 2 MAX-INT */MOD -> MAX-INT 2 MAX-INT T*/MOD }			( ** Fails ** )
{ MIN-INT 2 MIN-INT */MOD -> MIN-INT 2 MIN-INT T*/MOD }			( ** Fails ** )

{ 1 0 LSHIFT -> 1 }
{ 1 0 RSHIFT -> 1 }

CREATE BBUF 80 CHARS ALLOT

: EXPECT-TEST
   CR ." PLEASE TYPE UP TO 80 CHARACTERS:" CR
   BBUF 80 EXPECT
   CR ." RECEIVED: " [CHAR] " EMIT
   BBUF SPAN @ TYPE [CHAR] " EMIT CR
;

: EXPECT-TEST-1 SAVE-INPUT EXPECT-TEST RESTORE-INPUT ;				( ** rewritten using save/restore input as file and keyboard input is combined ** )
{ EXPECT-TEST-1 -> <true> }