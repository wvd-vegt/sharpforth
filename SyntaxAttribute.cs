﻿// -----------------------------------------------------------------------
// <copyright file="SyntaxAttribute.cs" company="G.W. van der Vegt">
// SharpForth is inspired by JonesForthInC v1.48 and bb4wForth.
// </copyright>
// -----------------------------------------------------------------------

namespace SharpForth
{
    using System;

    /// <summary>
    /// Attribute to specify Syntax of words.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class SyntaxAttribute : Attribute
    {
        /// <summary>
        /// The constructor is called when the attribute is set. 
        /// </summary>
        /// <param name="Synxtax"></param> 
        public SyntaxAttribute(String Synxtax)
        {
            this.syntax = Synxtax;
        }

        /// <summary>
        /// Keep a variable internally.
        /// </summary>
        protected String syntax;

        /// <summary>
        /// .. and show a copy to the outside world. 
        /// </summary>
        public String Syntax
        {
            get
            {
                return this.syntax;
            }
            set
            {
                this.syntax = value;
            }
        }
    }

}
